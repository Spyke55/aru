string ARU_addonVersion = "v. 1.0.19";

enum ARU_Conf_PlayerTeleport
{
	UiAcces,
	PlayerToPlayer,
	PlayerToArea
};

enum ARU_Manager_UiState
{
	DISABLED,
	ENABLED,
	ADMINONLY
};

enum ARU_EN
{	
	DISABLED,
	ENABLED
}

class ARU_Generic
{	
	protected ref static array<ARU_PlayerTeleportAreaComponent> m_aTeleportAreas = {};
	
	//------------------------------------------------------------------------------------------------
	static void InsertTeleportArea(ARU_PlayerTeleportAreaComponent playerTeleportAreaComponent)
	{
		if(!playerTeleportAreaComponent)
			return;
		
		m_aTeleportAreas.Insert(playerTeleportAreaComponent);					
		
		ARU_Debug.MyPrint("Player Teleport",string.Format("Zone Added: %1 (ID: %2)",playerTeleportAreaComponent.GetZoneName(),playerTeleportAreaComponent.GetZoneId()),ARU_EDiagMenu.ModMenu_PlayerTeleport);
	}
	
	//------------------------------------------------------------------------------------------------	
	static void RemoveTeleportArea(ARU_PlayerTeleportAreaComponent playerTeleportAreaComponent)
	{
		m_aTeleportAreas.RemoveItem(playerTeleportAreaComponent);
		
		ARU_Debug.MyPrint("Player Teleport",string.Format("Zone Removed: %1 (ID: %2)",playerTeleportAreaComponent.GetZoneName(),playerTeleportAreaComponent.GetZoneId()),ARU_EDiagMenu.ModMenu_PlayerTeleport);
	}
	
	//------------------------------------------------------------------------------------------------
	static array<ARU_PlayerTeleportAreaComponent> GetTeleportAreaArray()
	{
		return m_aTeleportAreas;
	}
	
	//------------------------------------------------------------------------------------------------
	static int GetTeleportAreaArrayCount()
	{
		return m_aTeleportAreas.Count();
	}	
	
	//------------------------------------------------------------------------------------------------
	static array<ARU_PlayerTeleportAreaComponent> GetTeleportAreaArrayById(int id)
	{
		array<ARU_PlayerTeleportAreaComponent> myArray = {};
		foreach (ARU_PlayerTeleportAreaComponent playerTeleportAreaComponent : m_aTeleportAreas)
		{
			if(playerTeleportAreaComponent)
			{				
				if(playerTeleportAreaComponent.GetZoneId()==id)	
					myArray.Insert(playerTeleportAreaComponent);
			}					
		};
		return myArray;
	}						
}