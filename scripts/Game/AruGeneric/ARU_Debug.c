enum ARU_EDiagMenu
{
	ModMenu = 0,
	ModMenu_AlwaysPrint,
	ModMenu_General,
	ModMenu_PlayerTeleport,
	ModMenu_VirtualArsenal
}

class ARU_Debug
{
	protected static bool m_bIsInitialized = false;
	
	//------------------------------------------------------------------------------------------------
	static void Register()
	{
		// init script, needs to run only once
		if(m_bIsInitialized)
			return;
		m_bIsInitialized = true;
				
		DiagMenu.RegisterMenu(SCR_DebugMenuID.ARU_DEBUGUI_MENU, "ARU Debug Print", "");
		DiagMenu.RegisterBool(SCR_DebugMenuID.ARU_DEBUGUI_GENERAL, "", "General", "ARU Debug Print");
		DiagMenu.RegisterBool(SCR_DebugMenuID.ARU_DEBUGUI_PLAYERTELEPORT, "", "Player Teleport", "ARU Debug Print");
		DiagMenu.RegisterBool(SCR_DebugMenuID.ARU_DEBUGUI_VIRTUALARSENAL, "", "Virtual Arsenal", "ARU Debug Print");
	}
	
	//------------------------------------------------------------------------------------------------
	static void UnRegister()
	{
		// init script, needs to run only once
		if(!m_bIsInitialized)
			return;
		m_bIsInitialized = false;
		
		DiagMenu.Unregister(SCR_DebugMenuID.ARU_DEBUGUI_GENERAL);
		DiagMenu.Unregister(SCR_DebugMenuID.ARU_DEBUGUI_PLAYERTELEPORT);
		DiagMenu.Unregister(SCR_DebugMenuID.ARU_DEBUGUI_VIRTUALARSENAL);
	}			
	
	//------------------------------------------------------------------------------------------------
	static void MyPrint(string title, string message, ARU_EDiagMenu diagType = ARU_EDiagMenu.ModMenu, LogLevel level = LogLevel.NORMAL)
	{
		bool myBool = true;
		
		switch (diagType)
		{
			case ARU_EDiagMenu.ModMenu_AlwaysPrint: 	{ myBool = true; break; }
			case ARU_EDiagMenu.ModMenu_General: 		{ myBool = DiagMenu.GetBool(SCR_DebugMenuID.ARU_DEBUGUI_GENERAL); break; }
			case ARU_EDiagMenu.ModMenu_PlayerTeleport: 	{ myBool = DiagMenu.GetBool(SCR_DebugMenuID.ARU_DEBUGUI_PLAYERTELEPORT); break; }
			case ARU_EDiagMenu.ModMenu_VirtualArsenal: 	{ myBool = DiagMenu.GetBool(SCR_DebugMenuID.ARU_DEBUGUI_VIRTUALARSENAL); break; }
		}
		
		if(myBool)
			Print(string.Format("[ARU] - %1 - %2 -> %3",SCR_Enum.GetEnumName(ARU_EDiagMenu,diagType),title,message),level);
	}
}