class ARU_GlobalRpl
{
	protected static IEntity m_MainRplEntity;
	protected static RplId m_MainRplId;
	
	static void SetMainLocalRplEntity(IEntity entity)
	{
		m_MainRplEntity = entity;
	}
	
	static void SetMainLocalRplId(RplId rplId)
	{
		m_MainRplId = rplId;
	}	
		
	static IEntity GetMainLocalRplEntity()
	{
		if(!m_MainRplEntity)
		{
			RplComponent itemRpl = ARU_Global.GetEntityRplComponentbyRplId(m_MainRplId);
			{
				ARU_Debug.MyPrint("Generic","Client - Failed to get RplComponent "+m_MainRplId,ARU_EDiagMenu.ModMenu_General);
				return null;			
			}		
			IEntity entity = itemRpl.GetEntity();	
			m_MainRplEntity = entity;			
		}
		
		return m_MainRplEntity;
	}	
	
	static RplId GetMainLocalRplId()
	{
		return m_MainRplId;
	}					
}