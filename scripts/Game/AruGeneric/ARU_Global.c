class ARU_Global
{
	static int GetPlayerId()
	{
		// Getting player entity
		IEntity playerEntity = SCR_PlayerController.GetLocalControlledEntity();			
		if(!playerEntity) return 0;	
		
		// Getting player Id
		int playerId = GetGame().GetPlayerManager().GetPlayerIdFromControlledEntity(playerEntity);
		return playerId;
	}	
	
	static bool IsInRunTime()
	{
		//--- Ignore when not in run-time (from SCR_EditableEntityComponent.c)
		if (!GetGame() || SCR_Global.IsEditMode())
		    return false;
		else
			return true;
	}
	
	static bool IsEqualToLocalControlledEntityId(RplId entityDestinationId)
	{
		IEntity entityDestination = ARU_Global.GetEntityByRplId(entityDestinationId);
		if(!entityDestination)
			return false;				
		
		if(SCR_PlayerController.GetLocalControlledEntity() == entityDestination)
			return true;
		else
			return false;		
	}	
	
	static RplComponent GetEntityRplComponentbyRplId(RplId entityRplId,bool logActive = false)
	{
		Managed anything = Replication.FindItem(entityRplId);		
		if(!anything)
		{
			if(logActive)
				ARU_Debug.MyPrint("Generic",string.Format("GetEntityRplComponentbyRplId - NotFoundById: %1 | %2",entityRplId.ToString(),Replication.FindOwner(entityRplId)),ARU_EDiagMenu.ModMenu_General);
			return null;				
		}					
		RplComponent itemRpl = RplComponent.Cast(anything);	
		return itemRpl;
	}
	
	static RplId GetEntityRplId(IEntity entity)
	{
		RplComponent comp = RplComponent.Cast(entity.FindComponent(RplComponent));
		
		if(!comp)
			return null;
		
		return comp.Id();
	}
	
	static IEntity GetEntityByRplId(RplId entityRplId,bool logActive = false)
	{		
		RplComponent itemRpl2 = GetEntityRplComponentbyRplId(entityRplId,logActive);
		if(!itemRpl2)	
		{
			if(logActive)
				ARU_Debug.MyPrint("Generic","GetEntityByRplId - Null",ARU_EDiagMenu.ModMenu_General);
			return null;			
		}
		IEntity entity2 = itemRpl2.GetEntity();	
		return entity2;			
	}	
}