
modded class SCR_ListBoxComponent
{
	// TODO Delete?
	/*
	// TempWorkarround fixed on 0.9.8
	//------------------------------------------------------------------------------------------------	
	override protected void _SetItemSelected(int item, bool selected, bool invokeOnChanged)
	{
		if (!m_bMultiSelection && selected)
			m_iCurrentItem = item;
		
		super._SetItemSelected(item, selected, invokeOnChanged);
	}
	*/	
	
	// Enable/Disbale functionality 
	protected bool m_bEnable = true;
		
	//------------------------------------------------------------------------------------------------
	void SetEnable(bool enable)
	{
		m_bEnable = enable;
	}
	
	//------------------------------------------------------------------------------------------------
	bool GetEnable()
	{
		return m_bEnable;
	}	
	
	//------------------------------------------------------------------------------------------------
	override protected void OnItemClick(SCR_ListBoxElementComponent comp)
	{
		if(m_bEnable)			
			super.OnItemClick(comp);
	}	
	
};