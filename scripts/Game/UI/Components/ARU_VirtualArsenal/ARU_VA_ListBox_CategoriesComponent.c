class ARU_VA_ListBox_CategoriesComponent : ARU_VA_ListBoxComponent
{		
	// -----------------------------------------------------------------------------------------
	int AddCategory(string item, ResourceName imageOrImageset, bool indicatorActive = false)
	{
		ARU_VA_CategoryElementComponent comp;
		
		int id = _AddCategory(item, imageOrImageset, indicatorActive, comp);
		
		return id;
	}	
	
	// -----------------------------------------------------------------------------------------
	void SetItemIndicator(int item, bool active)
	{
		if (item < 0 || item > m_aElementComponents.Count())
			return;

		Widget elementWidget = m_aElementComponents[item].GetRootWidget();
		if(!elementWidget)
			return;
		ARU_VA_CategoryElementComponent comp = ARU_VA_CategoryElementComponent.Cast(elementWidget.FindHandler(ARU_VA_CategoryElementComponent));
		if(!comp)
			return;		
		comp.SetIndicator(active);
	}
	
	//------------------------------------------------------------------------------------------------
	protected int _AddCategory(string item, ResourceName imageOrImageset, bool indicatorActive, out ARU_VA_CategoryElementComponent compOut,)
	{	
		// Create widget for this item
		// The layout can be provided either as argument or through attribute
		ResourceName selectedLayout = m_sElementLayout;
		if (!m_sElementLayout)
			return -1;
		Widget newWidget = GetGame().GetWorkspace().CreateWidgets(selectedLayout, m_wList);
		
		ARU_VA_CategoryElementComponent comp = ARU_VA_CategoryElementComponent.Cast(newWidget.FindHandler(ARU_VA_CategoryElementComponent));
		
		comp.SetText(item);
		comp.SetImage(imageOrImageset, string.Empty);
		comp.SetIndicator(indicatorActive);
		
		// Pushback to internal arrays
		int id = m_aElementComponents.Insert(comp);
		
		
		// Setup event handlers
		comp.m_OnClicked.Insert(OnItemClick);
		
		// Set up explicit navigation rules for elements. Otherwise we can't navigate
		// Through separators when we are at the edge of scrolling if there is an element
		// directly above/below the list box which intercepts focus
		string widgetName = this.GetUniqueWidgetName();
		newWidget.SetName(widgetName);
		if (m_aElementComponents.Count() > 1)
		{
			Widget prevWidget = m_aElementComponents[m_aElementComponents.Count() - 2].GetRootWidget();
			prevWidget.SetNavigation(WidgetNavigationDirection.DOWN, WidgetNavigationRuleType.EXPLICIT, newWidget.GetName());
			newWidget.SetNavigation(WidgetNavigationDirection.UP, WidgetNavigationRuleType.EXPLICIT, prevWidget.GetName());
		}
		
		compOut = comp;
		
		return id;
	}	
}

class ARU_VA_ListBox_LoadoutAreaCategoriesComponent : ARU_VA_ListBox_CategoriesComponent
{	
	
	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		super.WorkbenchPreview(w);						
		
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewIcon = "{FB0C5ADB0235B940}UI/Textures/InventoryIcons/InventorySlot_blouse.edds";
		
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		
		SetItemBlocked(3,true);
	}	
	
	// -----------------------------------------------------------------------------------------
	void SetItemBlocked(int item, bool active)
	{
		if (item < 0 || item > m_aElementComponents.Count())
			return;

		Widget elementWidget = m_aElementComponents[item].GetRootWidget();
		if(!elementWidget)
			return;
		ARU_VA_LoadoutAreaCategoryElementComponent comp = ARU_VA_LoadoutAreaCategoryElementComponent.Cast(elementWidget.FindHandler(ARU_VA_LoadoutAreaCategoryElementComponent));
		if(!comp)
			return;		
		comp.SetBlocked(active);
	}		
}

class ARU_VA_ListBox_ItemsCategoriesComponent: ARU_VA_ListBox_CategoriesComponent
{
	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		super.WorkbenchPreview(w);						
		
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewIcon = "{A3D157619860A564}UI/Textures/Editor/Attributes/Arsenal/Attribute_Arsenal_Magazines.edds";
		
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
	}		
}

class ARU_VA_ListBox_AttachmentsCategoriesComponent: ARU_VA_ListBox_CategoriesComponent
{
	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		super.WorkbenchPreview(w);						
		
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewIcon = "{CB055708E982C0A5}UI/Textures/Editor/Attributes/Arsenal/Attribute_Arsenal_Optics.edds";
		
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,true);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
		AddCategory(string.Empty,previewIcon,false);
	}		
}