class ARU_VA_ListBox_AssetComponent : ARU_VA_ListBoxComponent
{	
	protected int m_iCurrentIndex = -1;					
	//------------------------------------------------------------------------------------------------
	void SetLastInteractedItemIndex(int index)
	{
		m_iCurrentIndex = index;
	}	
	
	//------------------------------------------------------------------------------------------------
	int GetLastInteractedItemIndex()
	{
		return m_iCurrentIndex;
	}	
	// TODO Delete?
	/*
	override protected void _SetItemSelected(int item, bool selected, bool invokeOnChanged)		
	{
		if (item >= 0)
			m_iCurrentItem = item;
		super._SetItemSelected(item,selected,invokeOnChanged);
	}
	*/
	
};

class ARU_VA_ListBox_LoadoutAreaComponent : ARU_VA_ListBox_AssetComponent
{

	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		super.WorkbenchPreview(w);						
		
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewPrefab = "";
		const string name = "Jacket US BDU";
		const array<ResourceName> blockedAreasIcons = {"{24B00F970F8CAF51}UI/Textures/InventoryIcons/InventorySlot_pants.edds","{38968D9D6BE9C6DC}UI/Textures/InventoryIcons/InventorySlot_vest.edds","{044C18D7A0102172}UI/Textures/InventoryIcons/InventorySlot_backpack.edds"};
		const array<bool> blockedAreasState = {false,true};
		
		AddLoadoutArea(previewPrefab,name,true,5.0,true,null,null);
		AddLoadoutArea(previewPrefab,name,true,5.5,false,{},{});
		AddLoadoutArea(previewPrefab,name,true,0,false,{},{});
		AddLoadoutArea(previewPrefab,name,true,100.0,true,{},{});
		AddLoadoutArea(previewPrefab,name,false,150.0,true,{},{});
		AddLoadoutArea(previewPrefab,name,true,225.0,true,blockedAreasIcons,{false,false,false});
		AddLoadoutArea(previewPrefab,name,true,225.0,true,blockedAreasIcons,{false,false,true});
		AddLoadoutArea(previewPrefab,name,false,225.0,true,blockedAreasIcons,{true,true,true});
	}
	
	//------------------------------------------------------------------------------------------------
	int AddLoadoutArea(ResourceName prefab,string text,bool enable = true, float resource = 0.0, bool enableResource = true, array<ResourceName> requiredAreasList = null,array<bool> requiredAreasStateList = null)
	{	
		ARU_VA_LoadoutAreaElementComponent comp;			// ARU-TOOD: Remove
		
		int id = _AddLoadoutArea(prefab,text,enable,resource,enableResource,requiredAreasList,requiredAreasStateList,comp);
		
		return id;
	}		
	
	//------------------------------------------------------------------------------------------------
	protected int _AddLoadoutArea(ResourceName prefab, string desc, bool enable, float resource, bool enableResource, array<ResourceName> requiredAreasList, array<bool> requiredAreasStateList, out ARU_VA_LoadoutAreaElementComponent compOut)
	{	
		// Create widget for this item
		// The layout can be provided either as argument or through attribute
		ResourceName selectedLayout = m_sElementLayout;
		if (!m_sElementLayout)
			return -1;
		Widget newWidget = GetGame().GetWorkspace().CreateWidgets(selectedLayout, m_wList);
		
		ARU_VA_LoadoutAreaElementComponent comp = ARU_VA_LoadoutAreaElementComponent.Cast(newWidget.FindHandler(ARU_VA_LoadoutAreaElementComponent));
			
		comp.SetDescription(desc);
		comp.SetPreview(prefab);
		comp.SetEnabled(enable);
		comp.SetBlockingAreaImage(requiredAreasList,requiredAreasStateList);
		comp.SetResource(resource);
		comp.ShowResource(enableResource);
		
		// Pushback to internal arrays
		int id = m_aElementComponents.Insert(comp);
		
		// Setup event handlers
		comp.m_OnClicked.Insert(OnItemClick);		
				
		// Set up explicit navigation rules for elements. Otherwise we can't navigate
		// Through separators when we are at the edge of scrolling if there is an element
		// directly above/below the list box which intercepts focus
		string widgetName = this.GetUniqueWidgetName();
		newWidget.SetName(widgetName);
		if (m_aElementComponents.Count() > 1)
		{
			Widget prevWidget = m_aElementComponents[m_aElementComponents.Count() - 2].GetRootWidget();
			prevWidget.SetNavigation(WidgetNavigationDirection.DOWN, WidgetNavigationRuleType.EXPLICIT, newWidget.GetName());
			newWidget.SetNavigation(WidgetNavigationDirection.UP, WidgetNavigationRuleType.EXPLICIT, prevWidget.GetName());
		}		
		
		compOut = comp;
		
		return id;
	}			
};

class ARU_VA_ListBox_AttachmentsAreaComponent : ARU_VA_ListBox_LoadoutAreaComponent
{
	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewPrefab = "";
		const string name = "Optic 1";
		const array<ResourceName> blockedAreasIcons = {"{24B00F970F8CAF51}UI/Textures/InventoryIcons/InventorySlot_pants.edds","{38968D9D6BE9C6DC}UI/Textures/InventoryIcons/InventorySlot_vest.edds","{044C18D7A0102172}UI/Textures/InventoryIcons/InventorySlot_backpack.edds"};
		const array<bool> blockedAreasState = {false,true};
		
		AddLoadoutArea(previewPrefab,name,true,5.0,true,null,null);
		AddLoadoutArea(previewPrefab,name,true,5.5,false,{},{});
		AddLoadoutArea(previewPrefab,name,true,0,false,{},{});
		AddLoadoutArea(previewPrefab,name,true,100.0,true,{},{});
		AddLoadoutArea(previewPrefab,name,false,150.0,true,{},{});
		AddLoadoutArea(previewPrefab,name,true,225.0,true,blockedAreasIcons,{false,false,false});
		AddLoadoutArea(previewPrefab,name,true,225.0,true,blockedAreasIcons,{false,false,true});
		AddLoadoutArea(previewPrefab,name,false,225.0,true,blockedAreasIcons,{true,true,true});
	}	
}

class ARU_VA_ListBox_ItemsComponent : ARU_VA_ListBox_AssetComponent
{	
	ref ScriptInvoker m_OnAdd = new ScriptInvoker();
	ref ScriptInvoker m_OnRemove = new ScriptInvoker();
		
	//------------------------------------------------------------------------------------------------
	override protected void WorkbenchPreview(Widget w)
	{
		super.WorkbenchPreview(w);						
		
		if (!SCR_Global.IsEditMode())
			return;
		
		const ResourceName previewPrefab = "";
		const string name = "5.56x45mm 30rnd STANAG Mag";
		
		AddStorageItem(previewPrefab,name,0,0,true,true,10.0,true);
		AddStorageItem(previewPrefab,name,0,15,true,true,10.0,true);
		AddStorageItem(previewPrefab,name,0,5,false,false,15.0,true);
		AddStorageItem(previewPrefab,name,0,10,false,true,20.0,true);
		AddStorageItem(previewPrefab,name,0,20,true,false,10.0,true);
		AddStorageItem(previewPrefab,name,0,200,true,true,10.0,false);
		AddStorageItem(previewPrefab,name,0,0,true,true,10.0,false);		
	}
		
	//------------------------------------------------------------------------------------------------
	int AddStorageItem(ResourceName prefab,string desc,int count,int countUser,bool AddEn, bool RemEn, float resource, bool enableResource)
	{	
		ARU_VA_ItemsElementComponent comp;			// ARU-TOOD: Remove
		
		int id = _AddStorageItem(prefab,desc,count,countUser,AddEn,RemEn,resource,enableResource,comp);
		
		return id;
	}	
	
	//------------------------------------------------------------------------------------------------
	void UpdateCount(int index,int count,int countUser,bool AddEn, bool RemEn, float resource, bool enableResource)
	{	
		if(m_aElementComponents.IsEmpty())
			return;
		ARU_VA_ItemsElementComponent comp = ARU_VA_ItemsElementComponent.Cast(m_aElementComponents[index]);
		if(!comp)
			return;
		comp.SetNumber(count);
		comp.SetNumberUser(countUser);
		comp.SetEnableAdd(AddEn);
		comp.SetEnableRemove(RemEn);
		comp.SetResource(resource);
		comp.ShowResource(enableResource);
	}		
	
	//------------------------------------------------------------------------------------------------
	protected int _AddStorageItem(ResourceName prefab, string desc, int num, int numUser, bool AddEn, bool RemEn, float resource, bool enableResource, out ARU_VA_ItemsElementComponent compOut)
	{	
		// Create widget for this item
		// The layout can be provided either as argument or through attribute
		ResourceName selectedLayout = m_sElementLayout;
		if (!m_sElementLayout)
			return -1;
		Widget newWidget = GetGame().GetWorkspace().CreateWidgets(selectedLayout, m_wList);
		
		ARU_VA_ItemsElementComponent comp = ARU_VA_ItemsElementComponent.Cast(newWidget.FindHandler(ARU_VA_ItemsElementComponent));
		if(!comp)
		{
			ARU_Debug.MyPrint("UI","Virtual Arsenal - Error - ARU_VA_ItemsElementComponent is Null",ARU_EDiagMenu.ModMenu_VirtualArsenal);
			return -1;
		}
		
		comp.SetDescription(desc);
		comp.SetPreview(prefab);
		comp.SetNumber(num);
		comp.SetNumberUser(numUser);
		comp.SetEnableAdd(AddEn);
		comp.SetEnableRemove(RemEn);
		comp.SetResource(resource);
		comp.ShowResource(enableResource);
		comp.m_OnAdd.Insert(OnAddClick);
		comp.m_OnRemove.Insert(OnRemoveClick);				
		
		// Pushback to internal arrays
		int id = m_aElementComponents.Insert(comp);
		
		// Setup event handlers
		comp.m_OnClicked.Insert(OnItemClick);		
				
		// Set up explicit navigation rules for elements. Otherwise we can't navigate
		// Through separators when we are at the edge of scrolling if there is an element
		// directly above/below the list box which intercepts focus
		string widgetName = this.GetUniqueWidgetName();
		newWidget.SetName(widgetName);
		if (m_aElementComponents.Count() > 1)
		{
			Widget prevWidget = m_aElementComponents[m_aElementComponents.Count() - 2].GetRootWidget();
			prevWidget.SetNavigation(WidgetNavigationDirection.DOWN, WidgetNavigationRuleType.EXPLICIT, newWidget.GetName());
			newWidget.SetNavigation(WidgetNavigationDirection.UP, WidgetNavigationRuleType.EXPLICIT, prevWidget.GetName());
		}		
		
		compOut = comp;
		
		return id;
	}	
	
	//------------------------------------------------------------------------------------------------
	ARU_VA_ItemsElementComponent GetSelectedItemElement()
	{
		SCR_ListBoxElementComponent comp0 = GetElementComponent(GetSelectedItem());
		if(!comp0)
			return null;
		ARU_VA_ItemsElementComponent comp = ARU_VA_ItemsElementComponent.Cast(comp0);
		return comp;
	}
	
	//------------------------------------------------------------------------------------------------
	void OnAddClick(ARU_VA_ItemsElementComponent clickedElement)
	{
		SetLastInteractedItemIndex(m_aElementComponents.Find(clickedElement));		
		m_OnAdd.Invoke(this,GetLastInteractedItemIndex());
	}
	
	//------------------------------------------------------------------------------------------------
	void OnRemoveClick(ARU_VA_ItemsElementComponent clickedElement)
	{
		SetLastInteractedItemIndex(m_aElementComponents.Find(clickedElement));
		m_OnRemove.Invoke(this,GetLastInteractedItemIndex());
	}	
};