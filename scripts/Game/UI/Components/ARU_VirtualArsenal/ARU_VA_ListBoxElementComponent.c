// TODO DELETE
class ARU_VA_ListBoxElementComponent : SCR_ListBoxElementComponent
{	
	
};

class ARU_VA_CategoryElementComponent : ARU_VA_ListBoxElementComponent
{		
	protected const string WIDGET_INDICATOR = "Indicator";
	
	//------------------------------------------------------------------------------------------------
	override void HandlerAttached(Widget w)
	{
		super.HandlerAttached(w);
	}
	
	//------------------------------------------------------------------------------------------------
	void SetDescription(string text)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(m_sWidgetTextName));

		if (w)
		{
			w.SetVisible(!text.IsEmpty());
			w.SetText(text);
		}			
	}
	
	//------------------------------------------------------------------------------------------------
	void SetIndicator(bool active)
	{
		ImageWidget w = ImageWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_INDICATOR));
		if (w)
		{
			if(active)
				w.SetColorInt(ARGB(255, 255, 255, 255));
			else
				w.SetColorInt(ARGB(0, 255, 255, 255));
		}		
	}				
};

class ARU_VA_LoadoutAreaCategoryElementComponent : ARU_VA_CategoryElementComponent
{	
	protected const string WIDGET_BLOCK = "Blocker";
		
	//------------------------------------------------------------------------------------------------
	void SetBlocked(bool active)
	{
		OverlayWidget w = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_BLOCK));
		if (w)
		{
			w.SetVisible(active);
		}		
	}							
};

class ARU_VA_ItemsCategoryElementComponent : ARU_VA_CategoryElementComponent
{	
				
};

class ARU_VA_ElementComponent : ARU_VA_ListBoxElementComponent
{	
	protected const string WIDGET_PREVIEW = "Preview";
	protected const string WIDGET_RESOURCE_DISPLAY = "SuppliesCost0";
	protected const string WIDGET_RESOURCE = "SuppliesText";
	
	//------------------------------------------------------------------------------------------------
	void SetDescription(string text)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(m_sWidgetTextName));
		if (w)
		{
			w.SetVisible(!text.IsEmpty());
			w.SetText(text);
		}			
	}		
	
	//------------------------------------------------------------------------------------------------
	void SetPreview(ResourceName prefab)
	{
		if(!prefab)
			return;
		if(prefab.IsEmpty())
			return;
		ItemPreviewWidget w = ItemPreviewWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_PREVIEW));
		ChimeraWorld world = ChimeraWorld.CastFrom(GetGame().GetWorld());
		ItemPreviewManagerEntity m_Manager = world.GetItemPreviewManager();
		m_Manager.SetPreviewItemFromPrefab(w, prefab, null, false);
	}	
		
	//------------------------------------------------------------------------------------------------
	void SetResource(float price)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_RESOURCE));
		if (w)
			w.SetText(price.ToString());
	}
	
	//------------------------------------------------------------------------------------------------
	void ShowResource(bool show)
	{
		FrameWidget w = FrameWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_RESOURCE_DISPLAY));
		if (w)
			w.SetVisible(show);
	}							
			
};

class ARU_VA_LoadoutAreaElementComponent : ARU_VA_ElementComponent
{	
	protected const string WIDGET_IMAGE = "RequiredAreas";
	
	//------------------------------------------------------------------------------------------------
	void SetBlockingAreaImage(array<ResourceName> requiredAreasList, array<bool> requiredAreasStateList)
	{
		if(!requiredAreasList)
			return;
		HorizontalLayoutWidget w = HorizontalLayoutWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_IMAGE));
		if (w)
		{
			w.SetVisible(!requiredAreasList.IsEmpty());	
			ARU_ImageRackComponent handler = ARU_ImageRackComponent.Cast(w.FindHandler(ARU_ImageRackComponent));
			handler.SetImages(requiredAreasList,requiredAreasStateList);
		}		
	}
};

class ARU_VA_ItemsElementComponent : ARU_VA_ElementComponent
{		
	protected const string WIDGET_BUTTON_ADD = "Add";
	protected const string WIDGET_BUTTON_REMOVE = "Remove";	
	protected const string WIDGET_NUMBER = "Number";
	protected const string WIDGET_NUMBER_USER = "NumberUser";	
					
	// Constants
	protected const string m_sInfinity = "∞";
	
	// Widgets
	protected ButtonWidget m_wButton;
			
	// Components
	SCR_ButtonImageComponent m_cAddComp;
	SCR_ButtonImageComponent m_cRemoveComp;
	
	// Script invokers
	ref ScriptInvoker m_OnAdd = new ScriptInvoker();
	ref ScriptInvoker m_OnRemove = new ScriptInvoker();		
	
	//------------------------------------------------------------------------------------------------
	override void HandlerAttached(Widget w)
	{
		super.HandlerAttached(w);
		ElementInit();
	}	
		
	//------------------------------------------------------------------------------------------------
	void ElementInit()
	{		
		m_wButton = ButtonWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_BUTTON_ADD));
		m_cAddComp = SCR_ButtonImageComponent.Cast(m_wButton.FindHandler(SCR_ButtonImageComponent));
		if(m_cAddComp)
			m_cAddComp.m_OnClicked.Insert(OnAddClick);
		
		m_wButton = ButtonWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_BUTTON_REMOVE));
		m_cRemoveComp = SCR_ButtonImageComponent.Cast(m_wButton.FindHandler(SCR_ButtonImageComponent));
		if(m_cRemoveComp)
			m_cRemoveComp.m_OnClicked.Insert(OnRemoveClick);		
	}		
		
	//------------------------------------------------------------------------------------------------
	void SetNumber(int num)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_NUMBER));
		
		if (w)
			if(num<0)
				w.SetText(m_sInfinity);
			else
				w.SetText(num.ToString());

	}
	
	//------------------------------------------------------------------------------------------------
	void SetNumberUser(int num)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_NUMBER_USER));
		
		if (w)
			if(num<0)
				w.SetText(m_sInfinity);
			else
				w.SetText(num.ToString());
	}
	
	//------------------------------------------------------------------------------------------------
	void OnAddClick()
	{
		m_OnAdd.Invoke(this);
	}
	
	//------------------------------------------------------------------------------------------------
	void OnRemoveClick()
	{
		m_OnRemove.Invoke(this);
	}
	
	//------------------------------------------------------------------------------------------------
	void SetEnableRemove(bool enabled)
	{
		m_cRemoveComp.SetEnabled(enabled);
	}
	
	//------------------------------------------------------------------------------------------------
	bool GetEnableRemove()
	{
		return m_cRemoveComp.IsEnabled();
	}		
	
	//------------------------------------------------------------------------------------------------
	void SetEnableAdd(bool enabled)
	{
		m_cAddComp.SetEnabled(enabled);	
	}
	
	//------------------------------------------------------------------------------------------------
	bool GetEnableAdd()
	{
		return m_cAddComp.IsEnabled();
	}
};