class ARU_ImageRackComponent : ScriptedWidgetComponent
{
	// Widgets
	protected Widget m_wRoot;
	
	[Attribute("", UIWidgets.ResourceNamePicker, "Layout element used", "layout")]
	protected ResourceName m_ElementLayout;
	
	protected const string WIDGET_IMAGE = "Image";
	
	protected ref array<ImageWidget> m_aImageWidgetList = {};
	
	//------------------------------------------------------------------------------------------------
	override void HandlerAttached(Widget w)
	{
		m_wRoot = w;
		super.HandlerAttached(w);
		// Default to show something when placed in layout editor.
		CreateImageWidget();
		CreateImageWidget();
	}
	
	//------------------------------------------------------------------------------------------------
	void SetImages(array<ResourceName> resourceNameList,array<bool> requiredAreasStateList)
	{
		DeleteAllImageWidgets();
		
		foreach(int index , ResourceName resourceName : resourceNameList)
		{
			if(requiredAreasStateList[index])
				CreateImageWidget(resourceName,Color.DarkRed);
			else
				CreateImageWidget(resourceName,Color.Gray);			
		}
	}
	
	//------------------------------------------------------------------------------------------------
	void CreateImageWidget(ResourceName resourceName = string.Empty,Color color = Color.White)
	{
		if (m_ElementLayout == string.Empty)
			return;
			
		Widget w = GetGame().GetWorkspace().CreateWidgets(m_ElementLayout, m_wRoot);
		if (!w)
			return;		
		
		ImageWidget imageWidget = ImageWidget.Cast(w.FindWidget(WIDGET_IMAGE));
		
		if(!resourceName.IsEmpty() && imageWidget)
			imageWidget.LoadImageTexture(0,resourceName);		
		
		imageWidget.SetColor(color);
		
		m_aImageWidgetList.Insert(imageWidget);
	}	
	
	//------------------------------------------------------------------------------------------------
	void DeleteAllImageWidgets()
	{
		foreach(ImageWidget imageWidget : m_aImageWidgetList)
		{
			if(imageWidget)
				imageWidget.RemoveFromHierarchy();	
		}
		m_aImageWidgetList.Clear();
	}	
	
};