class ARU_VA_CategoryInfoComponent : ScriptedWidgetComponent
{
	// ---- Protected ----
	
	// Widgets
	protected Widget m_wRoot;	
	protected const string WIDGET_WEIGHT = "WeightText";
	protected const string WIDGET_CAPACITY = "CapacityPercentageText";
	protected const string WIDGET_CAPACITY_PROGRESSBAR = "CapacityProgressBar";
	
	// ------------------------------- Public -----------------------------------
	
	//------------------------------------------------------------------------------------------------
	//! Finds ARU_VA_CategoryInfoComponent on a widget
	static ARU_VA_CategoryInfoComponent FindComponent(Widget w)
	{
		return ARU_VA_CategoryInfoComponent.Cast(w.FindHandler(ARU_VA_CategoryInfoComponent));
	}
	
	//------------------------------------------------------------------------------------------------
	override void HandlerAttached(Widget w)
	{
		m_wRoot = w;
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetWeight(float totalWeight)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_WEIGHT));
		if(!w)
			return;
		w.SetTextFormat("#AR-ValueUnit_Short_Kilograms", totalWeight);
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetCapacity(float percentage)
	{
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_CAPACITY));
		if(!w)
			return;
		w.SetText(percentage.ToString() + "%");
		SetCapacityProgressBar(percentage);
	}
	
	//------------------------------------------------------------------------------------------------
	void SetCapacityProgressBar(float percentage)
	{
		SizeLayoutWidget w = SizeLayoutWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_CAPACITY_PROGRESSBAR));
		if(!w)
			return;		
		SCR_WLibProgressBarComponent handler = SCR_WLibProgressBarComponent.Cast(w.FindHandler(SCR_WLibProgressBarComponent));
		if(!handler)
			return;			
		handler.SetValue(percentage);
	}		
	
};