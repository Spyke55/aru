modded enum ChimeraMenuPreset {
    ARU_VirtualArsenal_ID
}

//------------------------------------------------------------------------------------------------
class ARU_UI_VirtualArsenal: ARU_ChimeraMenuNormalBase
{	
	// Widgets and Components
	protected Widget m_wRoot;
	
	protected OverlayWidget m_ListBoxOverlay;
	protected OverlayWidget m_ListBoxAttachmentOverlay;
	protected OverlayWidget m_ListBoxAttachmentCategoryOverlay;
	protected OverlayWidget m_ListBoxItemsAuxOverlay;
	protected OverlayWidget m_ListBoxItemsAuxCategoriesOverlay;
	protected HorizontalLayoutWidget m_HorizontalLayoutWidgetAttachments
	protected RichTextWidget m_InfoRichTexWidget;
	protected OverlayWidget m_CategoryInfoOverlay;
	protected ARU_VA_ListBox_LoadoutAreaCategoriesComponent m_cListBoxItemsCategories;
	protected ARU_VA_ListBox_LoadoutAreaComponent m_cListBoxItems;
	protected ARU_VA_ListBox_AttachmentsCategoriesComponent m_cListBoxAttachmentsCategories;
	protected ARU_VA_ListBox_AttachmentsAreaComponent m_cListBoxAttachments;
	protected ARU_VA_ListBox_ItemsComponent m_cListBoxItemsAux;
	protected ARU_VA_ListBox_ItemsCategoriesComponent m_cListBoxItemsAuxCategories;	
	protected ARU_VA_CategoryInfoComponent m_cCategoryInfoComponent;
	protected ARU_InventoryReplaceStorageComponent m_cInventoryReplacementComp;
	protected SCR_InputButtonComponent m_Cancel;
	protected SCR_InputButtonComponent m_Undress;
	protected SCR_InputButtonComponent m_Disarm;
	protected SCR_NavigationBarUI m_pNavigationBar;
		
	// Layer Widgets Names	
	protected const string WIDGET_LISTBOX_ITEMS_CAT		 	= "ListBoxItemsCategories";
	protected const string WIDGET_LISTBOX_ITEMS 			= "ListBoxItems";
	protected const string WIDGET_LISTBOX_ITEMS_AUX 		= "ListBoxItemsAux";
	protected const string WIDGET_LISTBOX_ITEMS_AUX_CAT 	= "ListBoxItemsAuxCategories";
	protected const string WIDGET_LISTBOX_ATTACH_CAT		= "ListBoxAttachmentsCategories";
	protected const string WIDGET_LISTBOX_ATACH 			= "ListBoxAttachments";	
	protected const string WIDGET_CATEGORY_INFO				= "VirtualArsenalCategoryInfo0";
	protected const string WIDGET_RICHTEXT_INFO				= "InfoText";
	protected const string WIDGET_FOOTER					= "FooterHorizontalLayout";		// todo good name
	protected const string WIDGET_RESOURCE					= "ResourceText";
	protected const string WIDGET_FACTION_IMAGE				= "FactionImage";
	protected const string WIDGET_FACTION_NAME				= "FactionText";
	protected const string WIDGET_PLAYER_WEIGHT				= "PlayerWeightText";
	protected const string WIDGET_RESOURCE_DISPLAY			= "ResourceDisplay";
	
	protected const string WIDGET_NAVIGATION_CANCEL 		= "Cancel";
	protected const string WIDGET_NAVIGATION_UNDRESS		= "Undress";
	protected const string WIDGET_NAVIGATION_DISARM			= "Disarm";
	
	protected const string WIDGET_STORAGE_NAME 				= "StorageName";		
	
	// Variables
	protected IEntity m_eStorageOwner;	
	protected IEntity m_eStorageUser;
	
	protected BaseWeaponManagerComponent weaponManager;	
	protected SCR_InventoryStorageManagerComponent inventoryManager;
	protected EquipedLoadoutStorageComponent loadoutManager;
	protected SCR_CharacterControllerComponent m_cCharacterController;
	protected SCR_CharacterCameraHandlerComponent m_cCharacterCamera;
	protected SCR_CharacterInventoryStorageComponent m_cCharacterInventory;
	protected SCR_Faction m_faction;
	protected SCR_ResourceConsumer m_resourceConsumer;
	protected SCR_ResourcePlayerControllerInventoryComponent m_ResourcePlayerControllerInventoryComponent;
	
	protected InputManager m_cinputManager;
	
	protected string m_sInfoText;
	
	// Arsenal variables
	protected ref ARU_ArsenalConfig m_ArsenalConfig;
	protected ref map<ResourceName, IEntity> m_aCachedEntities = new map<ResourceName, IEntity>();
	
	// -- Reseted variables when UnselectAll();	
	protected ARU_Arsenal_Category m_CurrentItemCategory = null;
	protected ARU_Arsenal_Items m_CurrentItem = null;
	protected IEntity m_eCurrentItemEntity = null;
	protected IEntity m_eCharacterEquippedEntityOnSlot = null;
	
	protected ARU_Arsenal_Category m_CurrentItemAuxCategory = null;
	protected ARU_Arsenal_Category m_CurrentAttachmentCategory = null;
	protected ARU_Arsenal_Items m_CurrentItemAux = null;
	protected IEntity m_eCurrentItemAuxEntity = null;

	protected int m_iListBoxItemsCategories_index = -1;
	protected int m_iListBoxItems_index = -1;
	protected int m_iListBoxItems_index2 = -1;
	protected int m_iListBoxItemsAux_index = -1;
	protected int m_iListBoxItemsAuxCategories_index = -1;
	protected int m_iListBoxAttachmentsCategories_index = -1;
	// -- Reseted variables when UnselectAll();	
	
	protected ref array<ref ARU_Arsenal_Category> m_iItemsCategories_List = {};	
	protected ref array<ref ARU_Arsenal_Category> m_iItemsAuxCategories_List = {};
	protected ref array<ref ARU_Arsenal_Category> m_iAttachmentsCategories_List = {};
	
	protected bool m_bWasThirdPerson = false;
	
	// Flags
	protected bool m_bIsItemsAuxListboxShown = false;
	protected bool m_bIsAttachmentsListboxShown = false;
	protected bool m_bIgnoreSelected = true;
	protected bool m_bEquippedItemChangeWaiting = false;
	protected bool m_bEnabled = false;
	protected bool m_bEnableSupplySystem = true;
	
	// Preview
	CameraManager cameraManager;
	CameraBase savedCamera;	
	
	// Constants
	protected const string m_StringEmpty = "Empty";	
	protected const bool m_bOnlyIcons = true;	
	
	//################################################################################################
	// Initialization and Set, Get Methods
	//------------------------------------------------------------------------------------------------
	override void OnMenuOpen()
	{
		super.OnMenuOpen();
		
		m_wRoot = GetRootWidget();
		
		// Cancel
		m_Cancel = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_CANCEL, m_wRoot);
		if (m_Cancel)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_Cancel.GetRootWidget());			
			m_Cancel.m_OnActivated.Insert(OnCancel);
		}		
		
		// Undress
		m_Undress = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_UNDRESS, m_wRoot);
		if (m_Undress)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_Undress.GetRootWidget());			
			m_Undress.m_OnActivated.Insert(UndresCharacter);
		}		
		
		// Disarm
		m_Disarm = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_DISARM, m_wRoot);
		if (m_Disarm)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_Disarm.GetRootWidget());			
			m_Disarm.m_OnActivated.Insert(DisarmCharacter);
		}		
		
		// Listbox
		m_ListBoxOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ITEMS_CAT));											
		m_cListBoxItemsCategories = ARU_VA_ListBox_LoadoutAreaCategoriesComponent.Cast(m_ListBoxOverlay.FindHandler(ARU_VA_ListBox_LoadoutAreaCategoriesComponent));
		m_cListBoxItemsCategories.m_OnChanged.Insert(OnItemsCategoriesChange);
		
		m_ListBoxOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ITEMS));											
		m_cListBoxItems = ARU_VA_ListBox_LoadoutAreaComponent.Cast(m_ListBoxOverlay.FindHandler(ARU_VA_ListBox_LoadoutAreaComponent));
		m_cListBoxItems.m_OnChanged.Insert(OnItemsChange);
		
		m_ListBoxItemsAuxOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ITEMS_AUX));											
		m_cListBoxItemsAux = ARU_VA_ListBox_ItemsComponent.Cast(m_ListBoxItemsAuxOverlay.FindHandler(ARU_VA_ListBox_ItemsComponent));
		m_cListBoxItemsAux.m_OnChanged.Insert(OnItemsAuxChange);
		m_cListBoxItemsAux.m_OnAdd.Insert(OnAddClick);
		m_cListBoxItemsAux.m_OnRemove.Insert(OnRemoveClick);			
		
		m_ListBoxItemsAuxCategoriesOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ITEMS_AUX_CAT));											
		m_cListBoxItemsAuxCategories = ARU_VA_ListBox_ItemsCategoriesComponent.Cast(m_ListBoxItemsAuxCategoriesOverlay.FindHandler(ARU_VA_ListBox_ItemsCategoriesComponent));
		m_cListBoxItemsAuxCategories.m_OnChanged.Insert(OnItemsAuxCategoriesChange);
		
		m_HorizontalLayoutWidgetAttachments = HorizontalLayoutWidget.Cast(m_wRoot.FindAnyWidget("HorizontalLayoutAttachments"));
		m_HorizontalLayoutWidgetAttachments.SetVisible(false);
		m_ListBoxAttachmentCategoryOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ATTACH_CAT));											
		m_cListBoxAttachmentsCategories = ARU_VA_ListBox_AttachmentsCategoriesComponent.Cast(m_ListBoxAttachmentCategoryOverlay.FindHandler(ARU_VA_ListBox_AttachmentsCategoriesComponent));
		m_cListBoxAttachmentsCategories.m_OnChanged.Insert(OnAttachmentCategoriesChange);
		
		m_ListBoxAttachmentOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ATACH));											
		m_cListBoxAttachments = ARU_VA_ListBox_AttachmentsAreaComponent.Cast(m_ListBoxAttachmentOverlay.FindHandler(ARU_VA_ListBox_AttachmentsAreaComponent));
		m_cListBoxAttachments.m_OnChanged.Insert(OnAttachmentChange);		
		
		m_InfoRichTexWidget = RichTextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_RICHTEXT_INFO));	
		m_InfoRichTexWidget.SetText("");
		
		//CATEGORY INFO
		m_CategoryInfoOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_CATEGORY_INFO));											
		m_cCategoryInfoComponent = ARU_VA_CategoryInfoComponent.Cast(m_CategoryInfoOverlay.FindHandler(ARU_VA_CategoryInfoComponent));
		
		//NAVIGATION BAR
		HorizontalLayoutWidget wNaviBar = HorizontalLayoutWidget.Cast( m_wRoot.FindAnyWidget(WIDGET_FOOTER));
		m_pNavigationBar = SCR_NavigationBarUI.Cast( wNaviBar.FindHandler( SCR_NavigationBarUI ) );		
		m_pNavigationBar.m_OnAction.Insert(OnAction);	
		
		m_cinputManager = GetGame().GetInputManager();		 
	}	
	
	//------------------------------------------------------------------------------------------------
	override void OnMenuUpdate(float tDelta)
	{
		super.OnMenuUpdate(tDelta);
		if(m_cinputManager)
			m_cinputManager.ActivateContext("ARU_VA_Context");					
	}	
	
	//------------------------------------------------------------------------------------------------
	void InitMenu()
	{
		RplComponent itemRplComp = RplComponent.Cast(GetStorageUser().FindComponent(RplComponent));
		RplId entityDestinationId = itemRplComp.Id();				
		
		inventoryManager = SCR_InventoryStorageManagerComponent.Cast(GetStorageUser().FindComponent(SCR_InventoryStorageManagerComponent));
		weaponManager = BaseWeaponManagerComponent.Cast(GetStorageUser().FindComponent(BaseWeaponManagerComponent));
		loadoutManager = EquipedLoadoutStorageComponent.Cast(GetStorageUser().FindComponent(EquipedLoadoutStorageComponent));
		m_cInventoryReplacementComp = ARU_InventoryReplaceStorageComponent.Cast(GetStorageUser().FindComponent(ARU_InventoryReplaceStorageComponent));
		m_cCharacterCamera = SCR_CharacterCameraHandlerComponent.Cast(GetStorageUser().FindComponent(SCR_CharacterCameraHandlerComponent));
		m_cCharacterInventory = SCR_CharacterInventoryStorageComponent.Cast(GetStorageUser().FindComponent(SCR_CharacterInventoryStorageComponent));
		m_ResourcePlayerControllerInventoryComponent = SCR_ResourcePlayerControllerInventoryComponent.Cast(GetStorageUser().FindComponent(SCR_ResourcePlayerControllerInventoryComponent));
				
		SCR_ArsenalComponent arsenalComponent = SCR_ArsenalComponent.Cast(GetStorageOwner().FindComponent(SCR_ArsenalComponent));
		if (arsenalComponent)
			m_faction = arsenalComponent.GetAssignedFaction();	
		
		SetArsenalFactionHeader();
		
		SCR_ResourceComponent resourceComponent = SCR_ResourceComponent.FindResourceComponent(GetStorageOwner());		
		if (resourceComponent)		
			m_resourceConsumer = resourceComponent.GetConsumer(EResourceGeneratorID.DEFAULT, EResourceType.SUPPLIES);
		
		SCR_BaseSupportStationComponent supportStation = SCR_BaseSupportStationComponent.Cast(GetStorageOwner().FindComponent(SCR_BaseSupportStationComponent));
		if (supportStation)
			m_bEnableSupplySystem = null;
			//m_bEnableSupplySystem = supportStation.IsUsingSupplies();		
		// TODO: 1.1.0.8 Changelog
		
		if(!m_faction || !m_resourceConsumer || !supportStation)
			m_bEnableSupplySystem = false;
		
		UpdateTotalResources();
		UpdatePlayerTotalWeight();
		
		// Change to third person if not already in (temp)
		m_bWasThirdPerson = m_cCharacterCamera.IsInThirdPerson();
		m_cCharacterCamera.SetThirdPerson(true);
		//OpenVrArsenalCamera();

		// Weapon to back (temp for avoiding weapon problems)		
		m_cCharacterController = SCR_CharacterControllerComponent .Cast(GetStorageUser().FindComponent(SCR_CharacterControllerComponent));
		m_cCharacterController.SelectWeapon(null);	// TODO Improve. Crash on Client if you change a weapon fast once menu opened.
		
		// Preselect first category.
		ListboxPopulate_ItemsCategory();
		UpdateAllCategoryIndicators();
		if(m_cListBoxItemsCategories.GetItemCount()>0)
			m_cListBoxItemsCategories.SetItemSelected(0,true);
		
		// Script invokers
		inventoryManager.m_OnItemAddedInvoker.Insert(OnItemAdded);
		inventoryManager.m_OnItemRemovedInvoker.Insert(OnItemRemoved);
		m_cInventoryReplacementComp.m_OnReplacementEnd.Insert(OnReplacementEnd);		
		
		// Navigation
		m_pNavigationBar.SetAllButtonEnabled(false);
		m_pNavigationBar.FillFromConfig();
		NavigationBarUpdate();
		
		// Temp workarround
		m_cListBoxItems.SetEnable(false);
		m_bEnabled = false;
		GetGame().GetCallqueue().CallLater(EnableListBox, 2500); // in milliseconds	
		//EnableListBox();	
	}
	
	//------------------------------------------------------------------------------------------------
	void EnableListBox()
	{
		m_cListBoxItems.SetEnable(true);
		m_bEnabled = true;
	}
	
	//------------------------------------------------------------------------------------------------
	protected IEntity GetCacheEntityByArsenalItem(ARU_Arsenal_Items item)
	{
		if(!item)
			return null;
		return GetCacheEntityByResourceName(item.GetItemResourceName());
	}	
	
	//------------------------------------------------------------------------------------------------
	protected IEntity GetCacheEntityByResourceName(ResourceName resourceName)
	{
		if(resourceName.IsEmpty())
			return null;
		return m_aCachedEntities.Get(resourceName);
	}	
	
	//------------------------------------------------------------------------------------------------
	void NavigationBarUpdate()
	{
		if (!m_pNavigationBar)
			return;

		m_pNavigationBar.SetAllButtonEnabled(false);
		
		m_pNavigationBar.SetButtonEnabled("ButtonSelect",true);
		
		if(m_bIsItemsAuxListboxShown)
		{
			ARU_VA_ItemsElementComponent comp = m_cListBoxItemsAux.GetSelectedItemElement();
			if(comp)
			{
				m_pNavigationBar.SetButtonEnabled("ButtonAdd",comp.GetEnableAdd());
				m_pNavigationBar.SetButtonEnabled("ButtonRemove",comp.GetEnableRemove());				
			}
		}
		
/*
		if (m_bIsUsingGamepad)
		{
			NavigationBarUpdateGamepad();
			return;
		}
*/

	}		
	
	//------------------------------------------------------------------------------------------------
	void NavigationBarUpdateGamepad()
	{
		//m_pNavigationBar.SetAllButtonEnabled(false);
		//m_pNavigationBar.SetButtonEnabled("ButtonBack", true);
		//m_pNavigationBar.SetButtonEnabled("ButtonSelect", true);
	}
	
	
	/*
	//------------------------------------------------------------------------------------------------
	void OpenVrArsenalCamera()
	{
		Print("OpenVrArsenalCamera - init");
		// Camera Entity
		IEntity camEntity = GetGame().SpawnEntityPrefab(Resource.Load("{43C5673F3AB5F854}Prefabs/Editor/Camera/ManualCameraVrArsenal2.et"));		
		if(!camEntity)
  			return;		
		SCR_ManualCamera manCamEntity = SCR_ManualCamera.Cast(camEntity);
		if(!manCamEntity)
  			return;
				
		// Set camera pos and Angle
		//vector pos = Vector(124.82,2.1,115.91);
		vector angle = Vector(-45,-135,0);
		
		vector pos = GetStorageUser().GetOrigin();	
		pos[0] = pos[0] + 2;
		pos[1] = pos[1] + 2;		
		
		Print( manCamEntity.GetOrigin() );		
		
		manCamEntity.SetOrigin(pos);
		manCamEntity.SetAngles(angle);
		manCamEntity.SetFOVDegree(45);
		
		manCamEntity.AttachTo(GetStorageUser());
			
		Print( manCamEntity.GetOrigin() );							
		
		// Getting camera manager		
		cameraManager = GetGame().GetCameraManager();
		savedCamera = cameraManager.CurrentCamera();
		cameraManager.SetCamera(manCamEntity);
		
		Print("OpenVrArsenalCamera - end");
	}
	*/
	
	//------------------------------------------------------------------------------------------------
	void CloseVrArsenalCamera()
	{
		if(!cameraManager || !savedCamera)
			return;
		// Returning to the same camara that was being used before oppening MyPreviewCamera
		//cameraManager.SetCamera(savedCamera);	// Not working? why?
		cameraManager.SetPreviousCamera();
	}			
	
	//------------------------------------------------------------------------------------------------
	void SetArsenalConfig(ARU_ArsenalConfig conf)
	{
		ARU_ArsenalConfig arsenalConf = null;
	}
	
	//------------------------------------------------------------------------------------------------
	ARU_ArsenalConfig GetArsenalConfig()
	{
		return m_ArsenalConfig;
	}
	
	//------------------------------------------------------------------------------------------------
	protected void SetCurrentItem(ARU_Arsenal_Items item)
	{
		m_CurrentItem = item;
	}
	
	//------------------------------------------------------------------------------------------------
	protected ARU_Arsenal_Items GetCurrentItem()
	{
		return m_CurrentItem;
	}			
	
	//------------------------------------------------------------------------------------------------
	void SetStorageOwner(IEntity owner)
	{
		m_eStorageOwner = owner;
	}
	
	//------------------------------------------------------------------------------------------------
	IEntity GetStorageOwner()
	{
		return m_eStorageOwner;
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetStorageUser(IEntity user)
	{
		m_eStorageUser = user;						
	}	
	
	//------------------------------------------------------------------------------------------------
	IEntity GetStorageUser()
	{
		return m_eStorageUser;
	}	
	
	
	//################################################################################################
	// Auxiliar functions		
	
	//------------------------------------------------------------------------------------------------
	protected void OnItemAdded(IEntity ent, BaseInventoryStorageComponent storage)
	{		
		// Improve (TODO), not consistent should wait for a specific item.
		if(m_bEquippedItemChangeWaiting)
			UpdateAuxCategoryVisibility();
		UpdateAllCategoryIndicators();	//TODO Improve, only update the current slot category?	
		UpdateTotalResources();	
		UpdatePlayerTotalWeight();
		UpdateCurrentItemsAuxList(ent,storage);
	}
	
	//------------------------------------------------------------------------------------------------
	protected void OnItemRemoved(IEntity ent, BaseInventoryStorageComponent storage)
	{		
		UpdateAllCategoryIndicators();	//TODO Improve, only update the current slot category?	
		UpdateTotalResources();	
		UpdatePlayerTotalWeight();		
		UpdateCurrentItemsAuxList(ent,storage);
	}		
	
	//------------------------------------------------------------------------------------------------
	protected void UpdateCurrentItemsAuxList(IEntity entity, BaseInventoryStorageComponent storage)
	{		
		if(!m_CurrentItemAuxCategory || !m_bIsItemsAuxListboxShown)
			return;
		ResourceName storageResourceName = entity.GetPrefabData().GetPrefabName();		
		
		ARU_Arsenal_Category cat = m_iItemsAuxCategories_List[m_iListBoxItemsAuxCategories_index];
		array<ref ARU_Arsenal_Items> itemsList = cat.GetAllCategoryItems();
		IEntity storageEntity = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		if(!storageEntity)
			return;
		
		BaseInventoryStorageComponent storageComponent = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));
		if(!storageComponent)
			return;		
		
		// Populate Listbox		
		IEntity ent;
		int itemCount;
		float supplyCost;
		bool enable;
		bool aviable;
		foreach(int currentIndex, ARU_Arsenal_Items item : itemsList)
		{
			ent = GetCacheEntityByArsenalItem(item);
			itemCount = ARU_InventoryHelper.GetStorageItemCountByResource(storageEntity,item.GetItemResourceName()); 	// TODO: Specific storage count	
			GettSupplyInfoForItem(item.GetItemResourceName(),aviable,supplyCost);
			enable = (inventoryManager.CanInsertItemInStorage(ent,storageComponent) && aviable);							
			m_cListBoxItemsAux.UpdateCount(currentIndex,0,itemCount,enable,(itemCount>0),supplyCost,m_bEnableSupplySystem);
		}
		UpdateCategoryInfo();
		NavigationBarUpdate();		
	}
	
	//------------------------------------------------------------------------------------------------
	bool LoadConfig(ResourceName arsenalBaseConfig, ARU_ArsenalConfig arsenalConfig = null)
	{
		m_ArsenalConfig = null;
		ARU_ArsenalConfig myArsenalConfig;
		if(!arsenalBaseConfig)
		{
			ARU_Debug.MyPrint("VirtualArsenal","WARNING - Base config is null, LoadConfig will abort.",ARU_EDiagMenu.ModMenu_VirtualArsenal);
			return false;
		}			
		
		if(!arsenalConfig)
		{
			// Try to get Vanilla Arsenal items
			IEntity owner = GetStorageOwner();
			ARU_InventoryHelper.GetConfigFromArsenal(owner,arsenalBaseConfig,myArsenalConfig,m_aCachedEntities);
			arsenalConfig = myArsenalConfig;
		}			
		else
		{
			// Use custom config
			m_aCachedEntities = new map<ResourceName, IEntity>();
			array<ResourceName> resourceNameArray = arsenalConfig.GetAllArsenalItems();
			
			//ItemPreviewManager variables
			ChimeraWorld chimeraWorld = GetGame().GetWorld();
			ItemPreviewManagerEntity itemPreviewManagerEntity = chimeraWorld.GetItemPreviewManager();			
			
			foreach(ResourceName resourceName : resourceNameArray)
				m_aCachedEntities.Insert(resourceName,itemPreviewManagerEntity.ResolvePreviewEntityForPrefab(resourceName));		
		}
		
		// Clear listbox
		if(m_iItemsCategories_List.Count()>0)
			m_iItemsCategories_List.Clear();
		
		if(m_iItemsAuxCategories_List.Count()>0)
			m_iItemsAuxCategories_List.Clear();
		
		if(m_iAttachmentsCategories_List.Count()>0)
			m_iAttachmentsCategories_List.Clear();		
		
		array<ARU_Arsenal_Category> myCategoriesList = arsenalConfig.GetAllCategories();		
		foreach(ARU_Arsenal_Category cat : myCategoriesList)
		{
			// Weapons and Cloth Left side
			if(ARU_Arsenal_CategoryWeapon.Cast(cat) || ARU_Arsenal_CategoryCloth.Cast(cat))
				m_iItemsCategories_List.Insert(cat);
			// Storables and Attachemnts Right side
			else if(ARU_Arsenal_CategoryStorable.Cast(cat) || ARU_Arsenal_CategoryAttachment.Cast(cat))
				m_iItemsAuxCategories_List.Insert(cat);
			else if(ARU_Arsenal_CategoryAttachment.Cast(cat))
				m_iAttachmentsCategories_List.Insert(cat);
		}
		
		m_ArsenalConfig = arsenalConfig;
		return true;			
	}
	
	//------------------------------------------------------------------------------------------------
	protected void SetArsenalFactionHeader()
	{
		ImageWidget factionImageWidget = ImageWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_FACTION_IMAGE));
		TextWidget factionNameWidget = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_FACTION_NAME));
		if(m_faction)
		{
			SCR_FactionUIInfo uiInfo =  SCR_FactionUIInfo.Cast(m_faction.GetUIInfo());
						
			if(factionImageWidget)			
				factionImageWidget.LoadImageTexture(0,uiInfo.GetIconPath());			
						
			if(factionNameWidget)			
				factionNameWidget.SetText(uiInfo.GetFactionNameUpperCase());			
		}
		else
		{
			if(factionImageWidget)			
				factionImageWidget.LoadImageTexture(0,"{C8326EF8E990396A}UI/Textures/Editor/EditableEntities/Factions/EditableEntity_Faction_Base.edds");			
						
			if(factionNameWidget)			
				factionNameWidget.SetText(m_ArsenalConfig.GetDisplayName());			
		}

	}	
	
	//------------------------------------------------------------------------------------------------
	int FindEquippedItemInCurrentItemCategory(ARU_Arsenal_Category cat,out IEntity characterEquippedEntity, out IEntity characterEquippedEntityOnSlot)
	{
		characterEquippedEntity = null;
		characterEquippedEntityOnSlot = null;
		
		ARU_Arsenal_CategoryCloth clothCat = ARU_Arsenal_CategoryCloth.Cast(cat);
		ARU_Arsenal_CategoryWeapon weaponCat = ARU_Arsenal_CategoryWeapon.Cast(cat);
		ARU_Arsenal_CategoryAttachment attachmentCat = ARU_Arsenal_CategoryAttachment.Cast(cat);
		
		if(clothCat)
		{
			if (!loadoutManager)
				return -1;
			
			IEntity entity = loadoutManager.GetClothFromArea(clothCat.GetLoadoutAreaType().Type());
			if(!entity)
				return -1;
			characterEquippedEntityOnSlot = entity;
				
			int index = clothCat.FindResourceNameInCategory(entity.GetPrefabData().GetPrefabName());
			if(index>=0)		
				characterEquippedEntity = entity;		
			return index;						
		}
		else if(weaponCat)
		{
			if(!weaponManager)
				return -1;		
			
			int index = -1;
			
			array<WeaponSlotComponent> outSlots = {};
			weaponManager.GetWeaponsSlots(outSlots);
			
			string catWeaponType = weaponCat.GetWeaponSlotType();
			int catWeaponIndex = weaponCat.GetWeaponSlotIndex();
			
			array<WeaponSlotComponent> weaponSlotComp = {};
			foreach (WeaponSlotComponent weaponSlot : outSlots)
			{
				string type = weaponSlot.GetWeaponSlotType();				
				if(type == catWeaponType)
				{
					if(catWeaponType == "primary")
						if(weaponSlot.GetWeaponSlotIndex() != catWeaponIndex)
							continue;
					
					IEntity entity = weaponSlot.GetWeaponEntity();
					if(!entity)
						continue;
					characterEquippedEntityOnSlot = entity;
					index = cat.FindResourceNameInCategory(entity.GetPrefabData().GetPrefabName());
					if(index>=0)
					{				
						characterEquippedEntity = entity;
						break;									
					}
				}
			}	
				
			return index;			
		}
		else if(attachmentCat)
		{
			// For specific attachmentType look if this attachment is on list.
			// TODO ...
			
			IEntity entity = null;
			SCR_WeaponAttachmentsStorageComponent comp = SCR_WeaponAttachmentsStorageComponent.Cast(entity.FindComponent(SCR_WeaponAttachmentsStorageComponent));
		}	
	
		return -1;
	}		
		
	//------------------------------------------------------------------------------------------------
	protected IEntity GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth clothCat)
	{
		if (!loadoutManager || !clothCat)
			return null;
		
		IEntity entity = loadoutManager.GetClothFromArea(clothCat.GetLoadoutAreaType().Type());
		return entity;
	}	
	
	//------------------------------------------------------------------------------------------------	
	protected void UpdateAllCategoryIndicators()
	{
		IEntity currentItem = null;
		IEntity currentEquipped = null;
		int positionInList = -1;
		if(m_cListBoxItemsCategories.GetItemCount() != m_iItemsCategories_List.Count())
			return;
		
		// Updating bloqued indicator
		array<typename> blockedSlots = {};
		m_cCharacterInventory.GetBlockedSlots(blockedSlots);		
		
		// Updating occupied indicator
		foreach (int index, ARU_Arsenal_Category cat : m_iItemsCategories_List)
		{
			int pos = -1;
			ARU_Arsenal_CategoryCloth clothCat = ARU_Arsenal_CategoryCloth.Cast(cat);
			if(clothCat)			
				pos = blockedSlots.Find(clothCat.GetLoadoutAreaType().Type());			
			UpdateCategoryBlock((pos>=0),index);
			UpdateCategoryIndicator(cat,index);
		}			
	}
	
	//------------------------------------------------------------------------------------------------	
	protected void UpdateCategoryIndicator(ARU_Arsenal_Category cat, int listIndex)
	{
		IEntity currentItem = null;
		IEntity currentEquipped = null;
		FindEquippedItemInCurrentItemCategory(cat,currentItem,currentEquipped);		
		if(currentEquipped)
			m_cListBoxItemsCategories.SetItemIndicator(listIndex,true);
		else
			m_cListBoxItemsCategories.SetItemIndicator(listIndex,false);			
	}	
	
	//------------------------------------------------------------------------------------------------	
	protected void UpdateCategoryBlock(bool bloqued, int listIndex)
	{
		m_cListBoxItemsCategories.SetItemBlocked(listIndex,bloqued);			
	}
		
	//------------------------------------------------------------------------------------------------	
	protected void UpdateCategoryInfo()
	{	
		IEntity storageEntity = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		if(!storageEntity)
			return;		
		
		BaseInventoryStorageComponent storageComp = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));	
		if(!storageComp)
			return;		
		
		m_cCategoryInfoComponent.SetWeight(ARU_InventoryHelper.GetTotalRoundedUpWeight(storageComp));
		m_cCategoryInfoComponent.SetCapacity(ARU_InventoryHelper.GetOccupiedVolumePercentage(storageComp));
		
		UpdateTotalResources();
		UpdatePlayerTotalWeight();
	}	
	
	//------------------------------------------------------------------------------------------------	
	protected void UpdateTotalResources()
	{	
		HorizontalLayoutWidget w2 = HorizontalLayoutWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_RESOURCE_DISPLAY));
		if(w2)
			w2.SetVisible(m_bEnableSupplySystem);
		
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_RESOURCE));	
		if(w && m_resourceConsumer)
			w.SetText(m_resourceConsumer.GetAggregatedResourceValue().ToString());
	}	
		
	//------------------------------------------------------------------------------------------------	
	protected void UpdatePlayerTotalWeight()
	{	
		TextWidget w = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_PLAYER_WEIGHT));	
		
		float totalWeight = Math.Round(inventoryManager.GetTotalWeightOfAllStorages() * 100); // totalWeight * 100 here to not lose the weight precision
		
		totalWeight = totalWeight / 100;
		
		if(w && inventoryManager)
			w.SetTextFormat("#AR-ValueUnit_Short_Kilograms", totalWeight);
	}
	
	//------------------------------------------------------------------------------------------------	
	protected void GettSupplyInfoForItem(ResourceName resourceName, out bool aviable, out float supplyCost)
	{	
		aviable = true;
		supplyCost = 0.0;
		if(!m_bEnableSupplySystem || !m_faction || !m_resourceConsumer)
			return;
		
		supplyCost = ARU_InventoryHelper.GetItemSupplyCost(m_faction,resourceName,m_resourceConsumer);
		SCR_ResourceConsumtionResponse response = m_resourceConsumer.RequestAvailability(supplyCost, true);
		aviable = (response.GetReason() == EResourceReason.SUFFICIENT);
	}
	
	//################################################################################################
	// Populate
	
	protected void ListboxPopulate_ItemsCategory()
	{
		// Clean ListBox if not empty
		if(m_cListBoxItemsCategories.GetItemCount()>0)
			m_cListBoxItemsCategories.Clear();
			
		// Populate Listbox	
		IEntity currentItem = null;
		IEntity currentEquipped = null;
		foreach(ARU_Arsenal_Category cat : m_iItemsCategories_List)
		{
			currentItem = null;
			currentEquipped = null;
			FindEquippedItemInCurrentItemCategory(cat,currentItem,currentEquipped);
			bool indicatorActive = false;
			if(currentEquipped)
				indicatorActive = true;
			if(m_bOnlyIcons)
				m_cListBoxItemsCategories.AddCategory("",cat.GetCategoryPicture(),indicatorActive);
			else
				m_cListBoxItemsCategories.AddCategory(cat.GetCategoryName(),cat.GetCategoryPicture(),indicatorActive);					
		}
	}
	
	protected void ListboxPopulate_Items()
	{
		// Clean ListBox if not empty
		if(m_cListBoxItems.GetItemCount()>0)
			m_cListBoxItems.Clear();
		
		// Add Empty Row
		m_cListBoxItems.AddLoadoutArea("",m_StringEmpty,true,0.0,false,{});
		
		// Populate Listbox
		array<ref ARU_Arsenal_Items> itemsList = m_CurrentItemCategory.GetAllCategoryItems();
		bool enabled = true;
		array<typename> blockedSlots = {};
		array<bool> requiredAreasStateList = {};
		array<ResourceName> requiredAreasList = {};
		IEntity ent;
		float supplyCost;
		bool enable;
		bool aviable;		
		foreach(ARU_Arsenal_Items item : itemsList)
		{
			// Getting blocked areas
			blockedSlots = {};
			requiredAreasStateList = {};
			ent = GetCacheEntityByArsenalItem(item);			
			enabled = RequiredAreaOccupied(ent,blockedSlots,requiredAreasStateList);			
			
			// Getting blocked areas equivalent image
			requiredAreasList = {};
			foreach(typename type : blockedSlots)			
				requiredAreasList.Insert(GetAreaEquivalentImage(type));
			
			// Adding to ListBox
			// Check if can be equipped missing? TODO
			GettSupplyInfoForItem(item.GetItemResourceName(),aviable,supplyCost);
			enable = (enabled && aviable);
			m_cListBoxItems.AddLoadoutArea(item.GetItemResourceName(),ARU_InventoryHelper.GetInventoryItemUIInfoName(ent),enable,supplyCost,m_bEnableSupplySystem,requiredAreasList,requiredAreasStateList);
		}				
	}
	
	protected void ListboxPopulate_ItemsAux()
	{
		if(m_cListBoxItemsAux.GetItemCount()>0)
			m_cListBoxItemsAux.Clear();
		
		ARU_Arsenal_Category cat = m_iItemsAuxCategories_List[m_iListBoxItemsAuxCategories_index];
		array<ref ARU_Arsenal_Items> itemsList = cat.GetAllCategoryItems();
		IEntity storageEntity = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		if(!storageEntity)
			return;
		
		BaseInventoryStorageComponent storageComponent = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));
		if(!storageComponent)
			return;
		
		// Populate Listbox
		IEntity ent;
		int itemCount;
		float supplyCost;
		bool enable;
		bool aviable;		
		foreach(ARU_Arsenal_Items item : itemsList)
		{
			ent = GetCacheEntityByArsenalItem(item);
			itemCount = ARU_InventoryHelper.GetStorageItemCountByResource(storageEntity,item.GetItemResourceName()); 	// TODO: Specific storage count
			GettSupplyInfoForItem(item.GetItemResourceName(),aviable,supplyCost);
			enable = (inventoryManager.CanInsertItemInStorage(ent,storageComponent) && aviable);
			m_cListBoxItemsAux.AddStorageItem(item.GetItemResourceName(),ARU_InventoryHelper.GetInventoryItemUIInfoName(ent),0,itemCount,enable,(itemCount>0),supplyCost,m_bEnableSupplySystem);
		}	
	}	
	
	protected void ListboxPopulate_ItemsAuxCategory()
	{			
		if(m_cListBoxItemsAuxCategories.GetItemCount()>0)
			m_cListBoxItemsAuxCategories.Clear();			
				
		IEntity ent = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		
		if(!ent)
			return;
		
		// Populate Listbox
		foreach(ARU_Arsenal_Category cat : m_iItemsAuxCategories_List)
		{					
			if(m_bOnlyIcons)
				m_cListBoxItemsAuxCategories.AddCategory("",cat.GetCategoryPicture());
			else
				m_cListBoxItemsAuxCategories.AddCategory(cat.GetCategoryName(),cat.GetCategoryPicture());
		}
		
		if(m_cListBoxItemsAuxCategories.GetItemCount()>0)
			m_cListBoxItemsAuxCategories.SetItemSelected(0,true);		
	}
	
	protected void ListboxPopulate_AttachmentsCategory()
	{
		Print("ListboxPopulate_AttachmentsCategory");
		// Clean ListBox if not empty
		if(m_cListBoxAttachmentsCategories.GetItemCount()>0)
			m_cListBoxAttachmentsCategories.Clear();
			
		// Populate Listbox	
		IEntity currentItem = null;
		IEntity currentEquipped = null;
		foreach(ARU_Arsenal_Category cat : m_iAttachmentsCategories_List)
		{
			currentItem = null;
			currentEquipped = null;
			
			//FindEquippedItemInCurrentItemCategory(cat,currentItem,currentEquipped);
			bool indicatorActive = false;
			if(currentEquipped)
				indicatorActive = true;
			if(m_bOnlyIcons)
				m_cListBoxAttachmentsCategories.AddCategory("",cat.GetCategoryPicture(),indicatorActive);
			else
				m_cListBoxAttachmentsCategories.AddCategory(cat.GetCategoryName(),cat.GetCategoryPicture(),indicatorActive);					
		}
	}	
	
	protected void ListboxPopulate_Attachments()
	{
		// Clean ListBox if not empty
		if(m_cListBoxAttachments.GetItemCount()>0)
			m_cListBoxAttachments.Clear();			
		
		// Add Empty Row
		m_cListBoxAttachments.AddLoadoutArea("",m_StringEmpty,true,0.0,false,{});
		
		// Populate Listbox
		array<ref ARU_Arsenal_Items> itemsList = m_CurrentAttachmentCategory.GetAllCategoryItems();
		bool enabled = true;
		array<typename> blockedSlots = {};
		array<bool> requiredAreasStateList = {};
		array<ResourceName> requiredAreasList = {};
		IEntity ent;
		float supplyCost;
		bool enable;
		bool aviable;		
		foreach(ARU_Arsenal_Items item : itemsList)
		{
			
			// Getting blocked areas
			blockedSlots = {};
			requiredAreasStateList = {};
			ent = GetCacheEntityByArsenalItem(item);	
			/*		
			enabled = RequiredAreaOccupied(ent,blockedSlots,requiredAreasStateList);			
			
			// Getting blocked areas equivalent image
			requiredAreasList = {};
			foreach(typename type : blockedSlots)			
				requiredAreasList.Insert(GetAreaEquivalentImage(type));
			*/
			enabled = true;
			// Adding to ListBox
			// Check if can be equipped missing? TODO
			GettSupplyInfoForItem(item.GetItemResourceName(),aviable,supplyCost);
			enable = (enabled && aviable);
			m_cListBoxAttachments.AddLoadoutArea(item.GetItemResourceName(),ARU_InventoryHelper.GetInventoryItemUIInfoName(ent),enable,supplyCost,m_bEnableSupplySystem,requiredAreasList,requiredAreasStateList);
		}				
	}	
	
	////////// TODO, MOVE TO CONFIG? Now with the new changes i can do that diferent
	protected ResourceName GetAreaEquivalentImage(typename type)
	{
		ResourceName resourceName = string.Empty;
		
		foreach (ARU_Arsenal_Category cat : m_iItemsCategories_List)
		{
			ARU_Arsenal_CategoryCloth clothCat = ARU_Arsenal_CategoryCloth.Cast(cat);
			if(!clothCat)
				continue;
			if(clothCat.GetLoadoutAreaType().Type() == type)
			{
				resourceName = cat.GetCategoryPicture();
				break;
			}
		}
		
		return resourceName;	
	}	
	
	////////////////////
	//------------------------------------------------------------------------------------------------
	void OnAddClick(ARU_VA_ListBox_ItemsComponent listBoxInstance ,int interactedItem)
	{
		ARU_Arsenal_Category cat = m_iItemsAuxCategories_List[m_iListBoxItemsAuxCategories_index];	
		ref ARU_Arsenal_Items arsenalItem = cat.GetCategoryItem(interactedItem);		
		IEntity storageEntity = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		if(!storageEntity)
			return;		
		
		BaseInventoryStorageComponent storageComp = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));	
		if(!storageComp)
			return;
				
		BaseInventoryStorageComponent storageTo = inventoryManager.FindStorageForInsert( GetCacheEntityByArsenalItem(arsenalItem), storageComp, EStoragePurpose.PURPOSE_ANY );
		if(!storageTo)
			return;
		
		if(m_bEnableSupplySystem)
		{
			SCR_ResourceConsumtionResponse response = m_resourceConsumer.RequestConsumtion(ARU_InventoryHelper.GetItemSupplyCost(m_faction,arsenalItem.GetItemResourceName(),m_resourceConsumer));
			if(response.GetReason() != EResourceReason.SUFFICIENT)
				return;			
		}
		bool res = inventoryManager.TrySpawnPrefabToStorage(arsenalItem.GetItemResourceName(),storageTo);
	}
	
	//------------------------------------------------------------------------------------------------
	void OnRemoveClick(ARU_VA_ListBox_ItemsComponent listBoxInstance ,int interactedItem)
	{
		ARU_Arsenal_Category cat = m_iItemsAuxCategories_List[m_iListBoxItemsAuxCategories_index];	
		ref ARU_Arsenal_Items arsenalItem = cat.GetCategoryItem(interactedItem);
		IEntity ent = GetEquippedCategoryItem(ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory));
		
		BaseInventoryStorageComponent storageEntity = BaseInventoryStorageComponent.Cast(ent.FindComponent(BaseInventoryStorageComponent));	
		if(!storageEntity)
			return;	
				
		// TODO learn to use predicates and use inventoryManager.FindItem()
		array<IEntity> outItems = {};
		storageEntity.GetAll(outItems);
		IEntity item = ARU_InventoryHelper.GetStorageItemEntityByResource(ent,arsenalItem.GetItemResourceName());
		if(!item)
			return;
		
		// TODO
		// Refund compatible with resource system?
		
		RemoveItemFromUserStorage_Infinite(item);		
	}	
	
	//################################################################################################
	// Change Events		
	protected void OnItemsCategoriesChange(SCR_ListBoxComponent comp, int item, bool selected)
	{				
		if(m_iListBoxItemsCategories_index == item)
			return;
		// Update arsenal variables
		m_iListBoxItemsCategories_index = item;
		m_iListBoxItems_index = -1;
		m_iListBoxItems_index2 = -1;		
		m_CurrentItemCategory = m_iItemsCategories_List[m_iListBoxItemsCategories_index];
		m_CurrentItemAuxCategory = null;
		m_bIgnoreSelected = true;
		
		int positionInList = -1;
		
		// If this category is blocked don't populate the items listbox.
		array<typename> blockedSlots = {};
		m_cCharacterInventory.GetBlockedSlots(blockedSlots);	
		ARU_Arsenal_CategoryCloth clothCat = ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory);
		int pos = -1;
		if(clothCat)
			pos = blockedSlots.Find(clothCat.GetLoadoutAreaType().Type());		
		if(pos>=0)
		{
			// Clear ListBox if not empty
			if(m_cListBoxItems.GetItemCount()>0)
				m_cListBoxItems.Clear();
			SetAuxCategoryVisible(false);	
			m_bIgnoreSelected = false;
			m_cListBoxItems.SetAllItemsSelected(false,true);
			return;					
		}
		
		// Search of current equipped item in this category
		positionInList = FindEquippedItemInCurrentItemCategory(m_CurrentItemCategory,m_eCurrentItemEntity,m_eCharacterEquippedEntityOnSlot);
		
		// Update Items Listbox
		ListboxPopulate_Items();			
		m_ListBoxOverlay.SetVisible(true);
		
		//m_bItemCategoryChanged	= false;
		SetAuxCategoryVisible(false);
		SetAttachmentCategoryVisible(false);
		if(positionInList>=0)
			m_cListBoxItems.SetItemSelected(positionInList+1,true);
		else if(!m_eCharacterEquippedEntityOnSlot)
			m_cListBoxItems.SetItemSelected(0,true);
		else
		{
			m_bIgnoreSelected = false;
			m_cListBoxItems.SetAllItemsSelected(false,true);
		}			
	}
	
	protected void OnItemsChange(SCR_ListBoxComponent comp, int item, bool newSelected)
	{		
		
		if(m_iListBoxItems_index2 == item)
			return;		
		// Update arsenal variables
		m_iListBoxItems_index = item-1;
		m_iListBoxItems_index2 = item;
		
		// Save the current Item Entity in this slot.
		int positionInList = FindEquippedItemInCurrentItemCategory(m_CurrentItemCategory,m_eCurrentItemEntity,m_eCharacterEquippedEntityOnSlot);		
		if(!m_bIgnoreSelected)
		{
			// <Empty> Selected Or Replace
			bool isReplace = false;
			bool isEmptySelected = (m_iListBoxItems_index < 0);
			if(m_eCharacterEquippedEntityOnSlot)
			{
				if(isEmptySelected)
				{
					// Delete					
					RemoveItemFromUserStorage_Infinite(m_eCharacterEquippedEntityOnSlot);									
				}
				else
				{
					// Replace
					isReplace = true;									
				}
			}
			else
			{
				// No item currently equipped on slot --> No need to replace
				isReplace = false;			
			}
	
			
			// If the selected Listbox field is the <Empty>, we do nothing more.
			if(isEmptySelected)
			{
				SetAttachmentCategoryVisible(false);
				SetAuxCategoryVisible(false);
				UpdateAllCategoryIndicators();
				return;
			}				
	
			// Get new item selected on ListBox
			m_CurrentItem = m_CurrentItemCategory.GetCategoryItem(m_iListBoxItems_index);				
			if(!m_CurrentItem)
				return;
	
			ARU_Arsenal_CategoryCloth clothCat = ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory);
			
			if(isReplace && clothCat)
			{
				ReplaceUserStorage_Infinite(m_eCharacterEquippedEntityOnSlot,m_CurrentItem.GetItemResourceName());
			}
			else
			{
				int weaponSlotIndex = -1;
				ARU_Arsenal_CategoryWeapon weaponCat = ARU_Arsenal_CategoryWeapon.Cast(m_CurrentItemCategory);
				if(weaponCat)
				{
					weaponSlotIndex = weaponCat.GetWeaponSlotIndex();
					if(isReplace)
					{					
						int index = ARU_InventoryHelper.GetPrimarySlotIndexEmpty(weaponManager,weaponSlotIndex);						 
						if(index<0)
							RemoveItemFromUserStorage_Infinite(m_eCharacterEquippedEntityOnSlot);				
					}
				}
				
				if(weaponCat)
					EquipWeapon(m_CurrentItem.GetItemResourceName(),weaponSlotIndex);
				else if(clothCat)
					EquipCloth(m_CurrentItem.GetItemResourceName());			
			}			
		}
		else
		{
			if(m_iListBoxItems_index < 0)
				m_CurrentItem = null;
			else
				m_CurrentItem = m_CurrentItemCategory.GetCategoryItem(m_iListBoxItems_index);
			m_bIgnoreSelected = false;
			UpdateAuxCategoryVisibility();
		}			
		m_bEquippedItemChangeWaiting = true;	
	}
	
	//------------------------------------------------------------------------------------------------	
	protected void OnItemsAuxChange(SCR_ListBoxComponent comp, int item, bool newSelected)
	{
		if(!newSelected)
			return;	
		
		NavigationBarUpdate();
	}		

	//------------------------------------------------------------------------------------------------	
	protected void OnItemsAuxCategoriesChange(SCR_ListBoxComponent comp, int item, bool newSelected)
	{		
		if(!newSelected)
			return;		
		m_iListBoxItemsAuxCategories_index = item;
		m_CurrentItemAuxCategory = m_iItemsAuxCategories_List[item];
		ListboxPopulate_ItemsAux();
	}
	
	//------------------------------------------------------------------------------------------------
	protected void OnAttachmentCategoriesChange(SCR_ListBoxComponent comp, int item, bool newSelected)
	{				
		if(!newSelected)
			return;		
		m_iListBoxAttachmentsCategories_index = item;
		m_CurrentAttachmentCategory = m_iAttachmentsCategories_List[item];
		ListboxPopulate_Attachments();				
	}
	
	//------------------------------------------------------------------------------------------------
	protected void OnAttachmentChange(SCR_ListBoxComponent comp, int item, bool selected)
	{				
		if(!selected)
			return;	
		
		NavigationBarUpdate();
	}		
	
	//------------------------------------------------------------------------------------------------	
	protected bool RequiredAreaOccupied(IEntity entity, out array<typename> blockedSlots, out array<bool> blockedSlotsState)
	{
		blockedSlots = {};
		blockedSlotsState = {};
		bool enabled = true;
		
		if(!ARU_Arsenal_CategoryCloth.Cast(m_CurrentItemCategory))
			return enabled;
		
		if(!entity)
			return enabled;
		
		// Getting virtual entity from cache.
		BaseLoadoutClothComponent loadoutComponent = BaseLoadoutClothComponent.Cast(entity.FindComponent(BaseLoadoutClothComponent));
		if(!loadoutComponent)
			return enabled;
		
		// Getting blocked slots
		loadoutComponent.GetBlockedSlots(blockedSlots);	
		
		// Getting wich of the blocked slots are currently occupied by some entity.			
		foreach(typename type : blockedSlots)
		{
			IEntity ent = loadoutManager.GetClothFromArea(type);
			if(ent)	
			{
				enabled = false;
				blockedSlotsState.Insert(true);	
			}					
			else
				blockedSlotsState.Insert(false);					
		}			
		return enabled;	
	}	
	
	//------------------------------------------------------------------------------------------------
	// TODO must be improved?
	protected void UpdateAuxCategoryVisibility()
	{
		if(!GetCurrentItem())
			return;
		
		IEntityComponentSource entityComponentSource = SCR_BaseContainerTools.FindComponentSource(Resource.Load(GetCurrentItem().GetItemResourceName()),BaseInventoryStorageComponent);		
		if(entityComponentSource)
		{
			IEntityComponentSource entityComponentSource0 = SCR_BaseContainerTools.FindComponentSource(Resource.Load(GetCurrentItem().GetItemResourceName()),WeaponAttachmentsStorageComponent);		
			if(entityComponentSource0)
			{
				SetAttachmentCategoryVisible(true);
				SetAuxCategoryVisible(false);
				//ListboxPopulate_AttachmentsCategory();
			}
			else
			{
				if(!m_bIsItemsAuxListboxShown)
				{
					SetAttachmentCategoryVisible(false);
					SetAuxCategoryVisible(true);
					ListboxPopulate_ItemsAuxCategory();					
				}
			}
		}
		else
		{
			SetAttachmentCategoryVisible(false);
			SetAuxCategoryVisible(false);
		}
	}	
	
	//------------------------------------------------------------------------------------------------
	protected void SetAuxCategoryVisible(bool show)
	{
		m_bIsItemsAuxListboxShown = show;
		m_ListBoxItemsAuxOverlay.SetVisible(show);
		m_ListBoxItemsAuxCategoriesOverlay.SetVisible(show);
		m_CategoryInfoOverlay.SetVisible(show);
		if(show)
			UpdateCategoryInfo();
		NavigationBarUpdate();
	}
	
	//------------------------------------------------------------------------------------------------
	protected void SetAttachmentCategoryVisible(bool show)
	{
		show = false;		// TEMP disable
		m_bIsAttachmentsListboxShown = show;
		m_HorizontalLayoutWidgetAttachments.SetVisible(show);
		//m_CategoryInfoOverlay.SetVisible(show);
		//if(show)
		//	UpdateCategoryInfo();
		//NavigationBarUpdate();
	}	
	
	//################################################################################################
	// Equip
	
	protected void EquipWeapon(ResourceName prefab,int categoryType = -1)
	{
		AddItemToUserStorage_Infinite(prefab,1,categoryType);		
	}
	
	protected void EquipCloth(ResourceName prefab)
	{
		AddItemToUserStorage_Infinite(prefab,-1);
	}	
	
	//------------------------------------------------------------------------------------------------
	void AddItemToUserStorage_Infinite(ResourceName prefab,int actionType = 0, int categoryType = -1)
	{
		if(!prefab || !m_cInventoryReplacementComp)
			return;	
		
		if(m_bEnableSupplySystem)
		{
			SCR_ResourceConsumtionResponse response = m_resourceConsumer.RequestConsumtion(ARU_InventoryHelper.GetItemSupplyCost(m_faction,prefab,m_resourceConsumer));
			if(response.GetReason() != EResourceReason.SUFFICIENT)
				return;		
		}	
				
		m_cInventoryReplacementComp.SpawnAndEquipItem(prefab,actionType,categoryType);	
	}
	
	//------------------------------------------------------------------------------------------------
	void RemoveItemFromUserStorage_Infinite(IEntity entityToRemove)
	{		 
		if(!entityToRemove || !m_cInventoryReplacementComp)
			return;				
		
		m_cInventoryReplacementComp.DeleteItem(ARU_Global.GetEntityRplId(entityToRemove));	
	}
	
	//------------------------------------------------------------------------------------------------
	void ReplaceUserStorage_Infinite(IEntity currentStorageEntity,ResourceName newStorageResourceName)
	{
		if(!currentStorageEntity || !m_cInventoryReplacementComp)
			return;
		
		bool replacementStartedVest = m_cInventoryReplacementComp.TryReplaceOnlyStoragesPrefab(currentStorageEntity,newStorageResourceName);
	}		
	
	//------------------------------------------------------------------------------------------------
	void OnReplacementEnd(ARU_InventoryReplaceStorageComponent comp, ARU_InventoryReplaceStorageEnd code, array<RplId> outFailedEntitiesRplId, IEntity newStorageEntity = null)
	{
				
	}		
	
	//------------------------------------------------------------------------------------------------
	void OnAction( SCR_InputButtonComponent comp, string action, SCR_InventoryStorageBaseUI pParentStorage = null, int traverseStorageIndex = -1 )
	{
		switch (action)
		{
			case "ARU_VA_AddItem":
			{
				OnAddClick(null,m_cListBoxItemsAux.GetSelectedItem());
				break;
			}
			case "ARU_VA_RemoveItem":
			{
				OnRemoveClick(null,m_cListBoxItemsAux.GetSelectedItem());
				break;
			}											
			
		}
	}
	
	
	//------------------------------------------------------------------------------------------------
	void UndresCharacter()
	{		
		if(!m_bEnabled || !m_ArsenalConfig)
			return;
		
		array<ARU_Arsenal_Category> myCategoriesList = m_ArsenalConfig.GetCategoryByTypename(ARU_Arsenal_CategoryClothList);		
		foreach(ARU_Arsenal_Category cat : myCategoriesList)
			RemoveItemByArea(ARU_Arsenal_CategoryCloth.Cast(cat).GetLoadoutAreaType().Type());
		
		UpdateAllCategoryIndicators();
		UnselectAll();
	}
	
	//------------------------------------------------------------------------------------------------
	void DisarmCharacter()
	{		
		if(!m_bEnabled)
			return;		
		
		array<WeaponSlotComponent> outSlots = {};
		weaponManager.GetWeaponsSlots(outSlots);
		
		foreach (WeaponSlotComponent weaponSlot : outSlots)
		{
			IEntity entity = weaponSlot.GetWeaponEntity();
			if(entity)
				RemoveItemFromUserStorage_Infinite(entity);			
		}		
		
		UpdateAllCategoryIndicators();
		UnselectAll();
	}	
	
	//------------------------------------------------------------------------------------------------
	void UnselectAll()
	{		
		if(!m_cListBoxItemsCategories)
			return;		
		m_cListBoxItemsCategories.SetItemSelected(m_cListBoxItemsCategories.GetSelectedItem(),false,true);
		m_ListBoxOverlay.SetVisible(false);
		SetAttachmentCategoryVisible(false);
		SetAuxCategoryVisible(false);		
		
		m_CurrentItemCategory = null;
		m_CurrentItem = null;
		m_eCurrentItemEntity = null;
		m_eCharacterEquippedEntityOnSlot = null;
		
		m_CurrentItemAuxCategory = null;
		m_CurrentItemAux = null;
		m_eCurrentItemAuxEntity = null;
	
		m_iListBoxItemsCategories_index = -1;
		m_iListBoxItems_index = -1;
		m_iListBoxItems_index2 = -1;
		m_iListBoxItemsAux_index = -1;
		m_iListBoxItemsAuxCategories_index = -1;
	}		
	
	
	//------------------------------------------------------------------------------------------------
	void RemoveItemByArea(typename area)
	{
		if(!loadoutManager)
			return;
		IEntity ent = loadoutManager.GetClothFromArea(area);
		if(ent)
			RemoveItemFromUserStorage_Infinite(ent);
	}
	
	//################################################################################################
	// Exit	
	//------------------------------------------------------------------------------------------------
	override protected void OnCancel()
	{
		CloseVrArsenalCamera();
		m_cCharacterCamera.SetThirdPerson(m_bWasThirdPerson);		
		super.OnCancel();		
	}	 			
	
};