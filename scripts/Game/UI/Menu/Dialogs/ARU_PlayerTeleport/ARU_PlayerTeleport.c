modded enum ChimeraMenuPreset {
    ARU_PlayerTeleport_ID
}

//------------------------------------------------------------------------------------------------
class ARU_UI_PlayerTeleport: ARU_ChimeraMenuBase
{	
	// Widgets and Components
	protected Widget m_wRoot;
	protected TextWidget m_wTextPlayers;	
	protected TextWidget m_wTextZones;	
	
	protected SCR_InputButtonComponent m_Confirm;
	protected SCR_InputButtonComponent m_Cancel;
	protected OverlayWidget m_ListBoxOverlay;
	protected OverlayWidget m_ListBoxOverlay2;
	protected SCR_ListBoxComponent m_ListBoxComponent;	
	protected SCR_ListBoxComponent m_ListBoxComponent2;	
	protected OverlayWidget m_wNoPlayers;	
	protected OverlayWidget m_wNoZones;	
	protected VerticalLayoutWidget m_wTabView;
	protected SCR_TabViewComponent m_cTabViewComponent;
	
	// Layer Widgets Names
	const string WIDGET_NAVIGATION_CANCEL = "Cancel";
	const string WIDGET_NAVIGATION_CONFIRM = "Confirm";
	const string WIDGET_LISTBOX_PLAYER = "PlayerListBox";		
	const string WIDGET_LISTBOX_ZONE = "ZoneListBox";
	const string WIDGET_TEXTBOX_PLAYERCOUNT = "PlayerCount_Count";
	const string WIDGET_TEXTBOX_ZONECOUNT = "ZonesCount_Count";
	const string WIDGET_OVERLAY_NOPLAYERS = "NoPlayers";
	const string WIDGET_OVERLAY_NOZONES = "NoZones";
	const string WIDGET_TAB_VIEW = "TabViewRoot0";
	
	// Variables	
	protected ref array<int> playerIdsList = {};
	protected ref array<ARU_PlayerTeleportAreaComponent> zoneList = {};
	protected PlayerManager playerManager;	
	protected bool m_bUpdate;	
	protected float m_fTimerLowFreqUpdate;
	int localPlayerId;
	protected ARU_PlayerTeleportAccesComponent m_playerTeleportAccesComponent;
	protected int currentTab = -1;
	protected int prevTab = -1;
	protected IEntity m_Owner;
		
	// Constants
	const float m_iSpawnMinDist = -2;
	const float m_iSpawnMaxDist = 3;
	const float m_fTimerUpdateList = 5.0;
	const float m_fTeleportOffsetAngle = 180.0;
	
	const int m_iTabNull = 0;
	const int m_iTabPtP = 1;
	const int m_iTabPtZ = 2;
	
	//------------------------------------------------------------------------------------------------
	IEntity GetOwner()
	{
		return m_Owner;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetOwner(IEntity owner)
	{
		m_Owner = owner;
	}	
	
	//------------------------------------------------------------------------------------------------
	override void OnMenuOpen()
	{
		super.OnMenuOpen();
		
		m_wRoot = GetRootWidget();
		
		// Id
		localPlayerId = GetGame().GetPlayerManager().GetPlayerIdFromControlledEntity(SCR_PlayerController.GetLocalControlledEntity());	
		
		// Confirm
		m_Confirm = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_CONFIRM, m_wRoot);
		if (m_Confirm)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_Confirm.GetRootWidget());			
			m_Confirm.m_OnActivated.Insert(OnConfirmation);
		}
		
		// Cancel
		m_Cancel = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_CANCEL, m_wRoot);
		if (m_Cancel)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_Cancel.GetRootWidget());			
			m_Cancel.m_OnActivated.Insert(OnCancel);
		}				
		
		// TabView
		m_wTabView = VerticalLayoutWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_TAB_VIEW));	
		m_cTabViewComponent = SCR_TabViewComponent.Cast(m_wTabView.FindHandler(SCR_TabViewComponent));						
	}
	
	//------------------------------------------------------------------------------------------------
	void Init()
	{
		m_playerTeleportAccesComponent = ARU_PlayerTeleportAccesComponent.Cast(GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if(!m_playerTeleportAccesComponent)
			return;	
		
		playerManager = GetGame().GetPlayerManager();
		if(!playerManager)
			return;
		
		// Blocking
		SetAcces(m_playerTeleportAccesComponent.GetAccesRules());
		
		// Enable/Disable Tabs	
		m_cTabViewComponent.SetTabVisible(m_iTabNull, false);
			
		m_cTabViewComponent.EnableTab(m_iTabPtP, m_playerTeleportAccesComponent.GetEnablePlayerToPlayer());
		m_cTabViewComponent.EnableTab(m_iTabPtZ, m_playerTeleportAccesComponent.GetEnablePlayerToArea());		
		
		if(!m_playerTeleportAccesComponent.GetEnablePlayerToPlayer())
		{
			if(!m_playerTeleportAccesComponent.GetEnablePlayerToArea())
				m_cTabViewComponent.ShowTab(m_iTabNull,true);
			else
				m_cTabViewComponent.ShowTab(m_iTabPtZ,true);
		}else{
			m_cTabViewComponent.ShowTab(m_iTabPtP,true);
		}			
	}
	
	void initTab(int tabIndex)
	{
		if(tabIndex == m_iTabPtP)
		{
			// Player Count
			m_wTextPlayers = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_TEXTBOX_PLAYERCOUNT));	
			
			// No Players Overlay
			m_wNoPlayers = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_OVERLAY_NOPLAYERS));			
			
			// Player ListBox
			m_ListBoxOverlay = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_PLAYER));
			m_ListBoxComponent = SCR_ListBoxComponent.Cast(m_ListBoxOverlay.FindHandler(SCR_ListBoxComponent));
			if (m_ListBoxComponent)
				UpdatePlayerList();			
		}
		else if (tabIndex == m_iTabPtZ)
		{
			// Player Count
			m_wTextZones = TextWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_TEXTBOX_ZONECOUNT));	
			
			// No Players Overlay
			m_wNoZones = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_OVERLAY_NOZONES));			
		
			// Zone ListBox
			m_ListBoxOverlay2 = OverlayWidget.Cast(m_wRoot.FindAnyWidget(WIDGET_LISTBOX_ZONE));
			m_ListBoxComponent2 = SCR_ListBoxComponent.Cast(m_ListBoxOverlay2.FindHandler(SCR_ListBoxComponent));
			if (m_ListBoxComponent2)
				UpdateZoneList();	
		}
	}
	
	//------------------------------------------------------------------------------------------------
	override void OnMenuUpdate(float tDelta)
	{		
		m_fTimerLowFreqUpdate += tDelta;
		if (m_fTimerLowFreqUpdate >= m_fTimerUpdateList)
		{
			m_bUpdate = true;
			m_fTimerLowFreqUpdate = 0;
		}else
			m_bUpdate = false;	
		
		//if(m_bUpdate)
		//	UpdatePlayerList();
		
		// If tab changed.
		if(m_cTabViewComponent)
		{
			currentTab = m_cTabViewComponent.GetShownTab();
			if(currentTab != prevTab)
			{
				prevTab = currentTab;
				initTab(currentTab);			
			}
		}
		
		if (m_Confirm && m_cTabViewComponent)
			if((currentTab == m_iTabPtP) && m_ListBoxComponent)
				m_Confirm.SetEnabled(m_ListBoxComponent.GetSelectedItem()>=0);
			else if((currentTab == m_iTabPtZ) && m_ListBoxComponent2)
				m_Confirm.SetEnabled(m_ListBoxComponent2.GetSelectedItem()>=0);
			else
				m_Confirm.SetEnabled(false);
		
	}
	
	//------------------------------------------------------------------------------------------------
	protected void UpdatePlayerList()
	{		
		//int currentSelected = m_ListBoxComponent.GetSelectedItem();
		//m_ListBoxComponent.ClearAll();	// BIS TODO see SCR_ListBoxComponent
		
		playerIdsList = {};
		playerManager = GetGame().GetPlayerManager();
		playerManager.GetPlayers(playerIdsList);						
				
		foreach (int currentIndex, int id : playerIdsList)
		{
			string name = playerManager.GetPlayerName(id);			
			if(id != localPlayerId)
				m_ListBoxComponent.AddItem(name);
		}	
					
		int count = playerIdsList.Count();
		m_wTextPlayers.SetText((count-1).ToString());
		
		if(m_wTextPlayers)
		{
			m_wTextPlayers.SetVisible(count>1);
		}			
		
		if(m_wNoPlayers)
		{
			m_wNoPlayers.SetVisible(count<=1);
		}		
		//m_ListBoxComponent.SetCurrentItem(currentSelected);	// BIS TODO see SCR_ListBoxComponent			
	}	
	
	//------------------------------------------------------------------------------------------------
	protected void UpdateZoneList()
	{		
		//int currentSelected = m_ListBoxComponent.GetSelectedItem();
		//m_ListBoxComponent.ClearAll();	// BIS TODO see SCR_ListBoxComponent
		int teleportId = m_playerTeleportAccesComponent.GetTeleportId();
		if(teleportId<0)
			zoneList = ARU_Generic.GetTeleportAreaArray();							
		else
			zoneList = ARU_Generic.GetTeleportAreaArrayById(teleportId);
			
		foreach (ARU_PlayerTeleportAreaComponent playerTeleportAreaComponent : zoneList)
		{
			if(playerTeleportAreaComponent)
			{
				string name = playerTeleportAreaComponent.GetZoneName();			
				int id = playerTeleportAreaComponent.GetZoneId();
				m_ListBoxComponent2.AddItem(name);
			}
			else
			{
				m_ListBoxComponent2.AddItem("Null");
			}
		}
		
		//m_ListBoxComponent.SetCurrentItem(currentSelected);	// BIS TODO see SCR_ListBoxComponent
		int count2 = zoneList.Count();
		m_wTextZones.SetText((count2).ToString());
		
		if(m_wNoZones)
		{
			m_wNoZones.SetVisible(count2<=0);
		}	
		
		if(m_wTextZones)
		{
			m_wTextZones.SetVisible(count2>0);
		}						
	}	
	
	//------------------------------------------------------------------------------------------------
	protected void OnConfirmation()
	{
		int index = m_cTabViewComponent.GetShownTab();
		if(index == m_iTabPtP)
			OnConfirmation_PlayerToPlayer();
		else if (index == m_iTabPtZ)
			OnConfirmation_PlayerToZone();
		
		// Closing GUI
		OnCancel();		
	}	
	
	void OnConfirmation_PlayerToPlayer()
	{
		// Player Entity
		IEntity teleportedPlayer = SCR_PlayerController.GetLocalControlledEntity();
		if(!teleportedPlayer)
			return;
		
		// Selected Destination Player
		int selectionIndex = m_ListBoxComponent.GetSelectedItem();
		if(selectionIndex < 0)
			return;
		
		// Destination Player Entity
		IEntity destionationPlayer = playerManager.GetPlayerControlledEntity(playerIdsList[selectionIndex+1]);	
		if(!destionationPlayer)
			return;
		
		// Destination Player In Vehicle
		// Getting destination player's vehicle
		IEntity destVehicle = GetPlayerVehicle(destionationPlayer);
		// Gettig current player's vehicle
		IEntity currVehicle = GetPlayerVehicle(teleportedPlayer);
		// If current player is on vehicle exit (TODO: GetOut and TP)
		if(currVehicle)
		{
			SCR_HintManagerComponent.GetInstance().ShowCustomHint("You can be teleported if you are inside a vehicle. Exit the vehicle!!", "ARU - Player Teleport - Warning", 3.0);
			return;
		};

		if(destVehicle)
		{
			// If in vehicle, the current player is moved in
			MoveInPlayerVehicle(teleportedPlayer,destVehicle);
		}else
		{
			// Destination Player on foot.				
			// Getting RelPos
			vector destinationVector = destionationPlayer.GetOrigin();		
			vector relPos = GetRelPos(destinationVector,GetDir(destionationPlayer),m_iSpawnMinDist);	
			
			// Setting Dir and new Pos
			teleportedPlayer.SetAngles(destionationPlayer.GetAngles());
			SCR_Global.TeleportPlayer(playerManager.GetPlayerIdFromControlledEntity(teleportedPlayer),relPos);
			
			// Getting destination player stance 		
			CharacterControllerComponent m_CharacterControllerDest = CharacterControllerComponent.Cast(destionationPlayer.FindComponent(CharacterControllerComponent));
			ECharacterStance stance = m_CharacterControllerDest.GetStance();
					
			// Setting teleported player stance 
			CharacterControllerComponent m_CharacterController = CharacterControllerComponent.Cast(teleportedPlayer.FindComponent(CharacterControllerComponent));
			m_CharacterController.SetStanceChange(ConvertStanceToInt(stance));
		};			
	}	
	
	void OnConfirmation_PlayerToZone()
	{
		// Player Entity
		IEntity teleportedPlayer = SCR_PlayerController.GetLocalControlledEntity();
		if(!teleportedPlayer)
			return;
		
		// Selected Destination Player
		int selectionIndex = m_ListBoxComponent2.GetSelectedItem();
		if(selectionIndex < 0)
			return;
		
		// Destination Player Entity
		IEntity destionationZone = zoneList[selectionIndex].GetOwner();	
		if(!destionationZone)
			return;
		
		// Gettig current player's vehicle
		IEntity currVehicle = GetPlayerVehicle(teleportedPlayer);
		// If current player is on vehicle exit (TODO: GetOut and TP)
		if(currVehicle)
		{
			SCR_HintManagerComponent.GetInstance().ShowCustomHint("You can be teleported if you are inside a vehicle. Exit the vehicle!!", "ARU - Player Teleport - Warning", 3.0);
			return;
		};		
		
		// Destination Player on foot.				
		// Getting RelPos
		vector destinationVector = destionationZone.GetOrigin();		
		vector relPos = GetRelPos(destinationVector,GetDir(destionationZone),m_iSpawnMinDist);	
		
		// Setting Dir and new Pos
		teleportedPlayer.SetAngles(destionationZone.GetAngles());
		SCR_Global.TeleportPlayer(playerManager.GetPlayerIdFromControlledEntity(teleportedPlayer),relPos);	
	}

	//------------------------------------------------------------------------------------------------
	IEntity GetPlayerVehicle(IEntity entity)
	{
		SCR_CompartmentAccessComponent compartmentAccess = SCR_CompartmentAccessComponent.Cast(entity.FindComponent(SCR_CompartmentAccessComponent));
		if(!compartmentAccess)
			return null;

		IEntity vehicle = compartmentAccess.GetVehicle();
		if (!vehicle)
			return null;
		
		return vehicle;
	}
	
	//------------------------------------------------------------------------------------------------
	bool MoveInPlayerVehicle(IEntity entity, IEntity vehicle)
	{
		SCR_CompartmentAccessComponent compartmentAccess = SCR_CompartmentAccessComponent.Cast(entity.FindComponent(SCR_CompartmentAccessComponent));
		if(!compartmentAccess)
			return false;

		//BaseCompartmentSlot slot = FindFreeAndAccessibleCompartment(vehicle, ECompartmentType.Cargo);
		
		bool res = compartmentAccess.MoveInVehicle(vehicle, ECompartmentType.CARGO);
		return res;
	}		
	
	//------------------------------------------------------------------------------------------------
	// Copied from SCR_AISetStance
	int ConvertStanceToInt(ECharacterStance stance)
	{
		switch (stance)
		{
			case ECharacterStance.STAND:
				return ECharacterStanceChange.STANCECHANGE_TOERECTED;
			case ECharacterStance.CROUCH:
				return ECharacterStanceChange.STANCECHANGE_TOCROUCH;
			case ECharacterStance.PRONE:
				return ECharacterStanceChange.STANCECHANGE_TOPRONE;
		}
		return 0;
	}		
	
	//------------------------------------------------------------------------------------------------
	protected float GetDir(IEntity entity)
	{
		float ret = 0.0;
		vector destinationDir = entity.GetYawPitchRoll();
		float yaw = destinationDir[0];		
		if(yaw<0)
			ret = 360+yaw;
		else
			ret = yaw;
		return ret;		
	}
	
	protected vector GetRelPos(vector pos,float dir, float dist)
	{
		vector relPos = vector.Zero;
		relPos[0] = (pos[0]) + (Math.Sin(dir)*dist);
		relPos[2] = (pos[2]) + (Math.Cos(dir)*dist);
		relPos[1] = (pos[1]);
		return relPos;
	}
	
	//------------------------------------------------------------------------------------------------
	override protected void OnCancel()
	{
		super.OnCancel();
	}			
	
};