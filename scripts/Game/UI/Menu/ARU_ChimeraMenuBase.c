//------------------------------------------------------------------------------------------------
class ARU_ChimeraMenuNormalBase : ChimeraMenuBase
{
	// Widgets and Components
	protected Widget m_wRootBase;
	protected TextWidget m_tTextWidget;	
	
	// Layer Widgets Names
	const string WIDGET_ARU_VERSION = "ARU_Version";	
	
	// Variables	
	protected string m_sAddonVersion = ARU_addonVersion;	
	protected ARU_Manager_UiState m_UiState = ARU_Manager_UiState.ENABLED;
	
	//------------------------------------------------------------------------------------------------
	override void OnMenuOpen()
	{		
		m_wRootBase = GetRootWidget();
		
		// ARU Addon Version update
		m_tTextWidget = TextWidget.Cast(m_wRootBase.FindAnyWidget(WIDGET_ARU_VERSION));
		if (m_tTextWidget)
			m_tTextWidget.SetText(m_sAddonVersion);																							
	}				
	
	//------------------------------------------------------------------------------------------------
	protected void OnCancel()
	{
		Close();
	}	
};

//------------------------------------------------------------------------------------------------
class ARU_ChimeraMenuBase : ARU_ChimeraMenuNormalBase
{
	// Widgets and Components
	protected OverlayWidget m_oContent;
	protected OverlayWidget m_oContentBlocked;
	protected OverlayWidget m_oFooter;
	
	protected SCR_InputButtonComponent m_CB_Cancel;
	
	// Layer Widgets Names
	const string WIDGET_OVERALAY_CONTENT = "ARU_Content";
	const string WIDGET_OVERALAY_CONTENTBLOCKED = "ARU_ContentBlocked";
	const string WIDGET_OVERALAY_FOOTER = "ARU_Footer";
	const string WIDGET_NAVIGATION_CB_CANCEL = "ARU_CB_Cancel";
	
	// Variables	
	
	//------------------------------------------------------------------------------------------------
	override void OnMenuOpen()
	{			
		super.OnMenuOpen();
			
		// Content Blocked Cancel
		m_CB_Cancel = SCR_InputButtonComponent.GetInputButtonComponent(WIDGET_NAVIGATION_CB_CANCEL, m_wRootBase);
		if (m_CB_Cancel)
		{
			GetGame().GetWorkspace().SetFocusedWidget(m_CB_Cancel.GetRootWidget());			
			m_CB_Cancel.m_OnActivated.Insert(OnCancel);
		}
		
		// ARU Content
		m_oContent = OverlayWidget.Cast(m_wRootBase.FindAnyWidget(WIDGET_OVERALAY_CONTENT));									
		
		// ARU Content Blocked
		m_oContentBlocked = OverlayWidget.Cast(m_wRootBase.FindAnyWidget(WIDGET_OVERALAY_CONTENTBLOCKED));		
		
		// ARU Footer
		m_oFooter = OverlayWidget.Cast(m_wRootBase.FindAnyWidget(WIDGET_OVERALAY_FOOTER));
						
		UpdateAcces();								
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetAcces(ARU_Manager_UiState uiState)
	{
		m_UiState = uiState;
		UpdateAcces();
	}
	
	//------------------------------------------------------------------------------------------------
	ARU_Manager_UiState GetAcces()
	{
		return m_UiState;
	}	
	
	//------------------------------------------------------------------------------------------------
	protected void UpdateAcces()
	{
		if (!m_oContent || !m_oContentBlocked || !m_oFooter)
			return;
		
		bool accesEnabled = false;
		
		switch (m_UiState)
		{
			case ARU_Manager_UiState.DISABLED: { accesEnabled = false; break;};
			case ARU_Manager_UiState.ENABLED: { accesEnabled = true; break;};
			case ARU_Manager_UiState.ADMINONLY: { accesEnabled = SCR_Global.IsAdmin(ARU_Global.GetPlayerId()); break; };
			default: {accesEnabled = false;};
		};				
		
		if(accesEnabled)
		{
			m_oContent.SetVisible(true);
			m_oContentBlocked.SetVisible(false);
			m_oFooter.SetVisible(true);
		}else
		{
			m_oContent.SetVisible(false);
			m_oContentBlocked.SetVisible(true);
			m_oFooter.SetVisible(false);
		}
	}		
};
