//------------------------------------------------------------------------------------------------
[BaseContainerProps(), SCR_BaseContainerLocalizedTitleField("m_sTabButtonContent")]
class ARU_Layout
{
	Widget m_wTab;
	
	[Attribute("", UIWidgets.ResourceNamePicker, "Layout element used", "layout")]
	ResourceName m_ElementLayout;
	
	[Attribute("LayoutName", UIWidgets.EditBox, "Content of the button: It can contain either name or image resource")]
	string m_sTabButtonContent;	
};

//------------------------------------------------------------------------------------------------
class ARU_ManageLayoutsComponent : ScriptedWidgetComponent
{
	[Attribute("", UIWidgets.Object, "")]
	protected ref array<ref ARU_Layout> m_aElements;
	
	[Attribute("0", UIWidgets.EditBox, "Element selected by default")]
	int m_iSelectedTab;	
	
	Widget m_wRoot;
	protected Widget m_wContentOverlay;
		
	//------------------------------------------------------------------------------------------------
	override void HandlerAttached(Widget w)
	{
		m_wRoot = w;
		super.HandlerAttached(w);
		Init();
	}
	
	//------------------------------------------------------------------------------------------------
	void Init()
	{
		// Prevent multiple initialization
		if (!m_wRoot)
			return;	
			
		//m_wContentOverlay = m_wRoot.FindAnyWidget("ContentOverlay");
		m_wContentOverlay = m_wRoot;
		
		if (!m_wContentOverlay)
			return;		

		if (!m_aElements)
			return;
		
		foreach (int i, ARU_Layout content : m_aElements)
		{
			//CreateTab(content);
			//if (m_bCreateAllTabsAtStart)
			CreateTabContent(content);
		}
		
		int realSelected = 0;
		ShowTab(realSelected);
	}	
	
	//------------------------------------------------------------------------------------------------
	void CreateTabContent(ARU_Layout content)
	{
		ResourceName path = content.m_ElementLayout;
		if (path == string.Empty)
			return;
		
		Widget w = GetGame().GetWorkspace().CreateWidgets(path, m_wContentOverlay);
		if (!w)
			return;
		
		OverlaySlot.SetVerticalAlign(w, LayoutVerticalAlign.Stretch);
		OverlaySlot.SetHorizontalAlign(w, LayoutHorizontalAlign.Stretch);
		content.m_wTab = w;
		
		int i = m_aElements.Find(content);
		if (i != m_iSelectedTab)
		{
			w.SetVisible(false);
		}
		else
		{
			FocusFirstWidget(w);
		}
	}
	
	//------------------------------------------------------------------------------------------------
	void ShowTab(int i)
	{				
		// Do not switch into invalid or disabled element
		if (i < 0 || i >= m_aElements.Count())
			return;

		// Deselect old tab, select new tab
		SelectIndex(false, m_iSelectedTab);
		m_iSelectedTab = i;
		SelectIndex(true, i);
	}
	
	//------------------------------------------------------------------------------------------------
	protected void SelectIndex(bool select, int i)
	{
		if (!m_aElements || i < 0 || i >= m_aElements.Count())
			return;
		
		ARU_Layout content = m_aElements[i];
		if (!content)
			return;		
		
		Widget tab = content.m_wTab;
		if (select)
		{
			if (tab)
			{
				tab.SetVisible(true);
				FocusFirstWidget(tab);
			}
			else
			{
				CreateTabContent(content);
			}
		}
		else
		{
			if (!tab)
				return;

			tab.RemoveFromHierarchy();
		}
	}	
	
	//------------------------------------------------------------------------------------------------
	void FocusFirstWidget(Widget w)
	{
		Widget child = w;
		while (child)
		{
			ButtonWidget button = ButtonWidget.Cast(child);
			if (button && button.IsEnabled())
			{
				GetGame().GetWorkspace().SetFocusedWidget(button);
				return;
			}
			
			child = child.GetChildren();
		}
	}		
	
};