[BaseContainerProps(), SCR_BaseEditorAttributeCustomTitle()]
class ARU_PlayerTeleportAccesRulesEditorAttribute : SCR_BaseFloatValueHolderEditorAttribute
{	
	override SCR_BaseEditorAttributeVar ReadVariable(Managed item, SCR_AttributesManagerEditorComponent manager)
	{
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return null;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return null;				

		int accesRulesIndex;
		ARU_Manager_UiState accesRules = myComp.GetAccesRules();
		switch (accesRules)
		{
			case ARU_Manager_UiState.ENABLED: { accesRulesIndex = 0; break; }
			case ARU_Manager_UiState.DISABLED: { accesRulesIndex = 1; break; }
			case ARU_Manager_UiState.ADMINONLY: { accesRulesIndex = 2; break; }
			default: { accesRulesIndex = 0; }
		}
		
		return SCR_BaseEditorAttributeVar.CreateInt(accesRulesIndex);
	}
	
	override void WriteVariable(Managed item, SCR_BaseEditorAttributeVar var, SCR_AttributesManagerEditorComponent manager, int playerID)
	{
		if (!var)
			return;
		
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return;	
		
		int accesRulesIndex = var.GetInt();
		
		switch (accesRulesIndex)
		{
			case 0: { myComp.SetAccesRules(ARU_Manager_UiState.ENABLED); break; }
			case 1: { myComp.SetAccesRules(ARU_Manager_UiState.DISABLED); break; }
			case 2: { myComp.SetAccesRules(ARU_Manager_UiState.ADMINONLY); break; }
			default: { myComp.SetAccesRules(ARU_Manager_UiState.ENABLED); }
		}
	}
};

[BaseContainerProps(), SCR_BaseEditorAttributeCustomTitle()]
class ARU_EnablePlayerTeleportToPlayerEditorAttribute : SCR_BaseEditorAttribute
{
	//------------------------------------------------------------------------------------------------
	override SCR_BaseEditorAttributeVar ReadVariable(Managed item, SCR_AttributesManagerEditorComponent manager)
	{
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return null;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return null;				

		return SCR_BaseEditorAttributeVar.CreateBool(myComp.GetEnablePlayerToPlayer());
	}
	
	//------------------------------------------------------------------------------------------------
	override void WriteVariable(Managed item, SCR_BaseEditorAttributeVar var, SCR_AttributesManagerEditorComponent manager, int playerID)
	{
		if (!var)
			return;
		
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return;	
		
		myComp.SetEnablePlayerToPlayer(var.GetBool());
	}
}

[BaseContainerProps(), SCR_BaseEditorAttributeCustomTitle()]
class ARU_EnablePlayerTeleportToAreaEditorAttribute : SCR_BaseEditorAttribute
{
	//------------------------------------------------------------------------------------------------
	override SCR_BaseEditorAttributeVar ReadVariable(Managed item, SCR_AttributesManagerEditorComponent manager)
	{
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return null;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return null;				

		return SCR_BaseEditorAttributeVar.CreateBool(myComp.GetEnablePlayerToArea());
	}
	
	//------------------------------------------------------------------------------------------------
	override void WriteVariable(Managed item, SCR_BaseEditorAttributeVar var, SCR_AttributesManagerEditorComponent manager, int playerID)
	{
		if (!var)
			return;
		
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return;	
		
		myComp.SetEnablePlayerToArea(var.GetBool());
	}
}

[BaseContainerProps(), SCR_BaseEditorAttributeCustomTitle()]
class ARU_TeleportIdEditorAttribute : SCR_BaseValueListEditorAttribute
{
	override SCR_BaseEditorAttributeVar ReadVariable(Managed item, SCR_AttributesManagerEditorComponent manager)
	{
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return null;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return null;
		
		return SCR_BaseEditorAttributeVar.CreateInt(myComp.GetTeleportId());
		
	}	
	override void WriteVariable(Managed item, SCR_BaseEditorAttributeVar var, SCR_AttributesManagerEditorComponent manager, int playerID)
	{
		if (!var)
			return;
		
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAccesComponent myComp = ARU_PlayerTeleportAccesComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAccesComponent));
		if (!myComp)
			return;	
		
		myComp.SetTeleportId(var.GetInt());		
	}
};

[BaseContainerProps(), SCR_BaseEditorAttributeCustomTitle()]
class ARU_TeleportAreaIdEditorAttribute : SCR_BaseValueListEditorAttribute
{
	override SCR_BaseEditorAttributeVar ReadVariable(Managed item, SCR_AttributesManagerEditorComponent manager)
	{
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return null;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAreaComponent myComp = ARU_PlayerTeleportAreaComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAreaComponent));
		if (!myComp)
			return null;
		
		return SCR_BaseEditorAttributeVar.CreateInt(myComp.GetZoneId());
		
	}	
	override void WriteVariable(Managed item, SCR_BaseEditorAttributeVar var, SCR_AttributesManagerEditorComponent manager, int playerID)
	{
		if (!var)
			return;
		
		SCR_EditableEntityComponent editableEntity = SCR_EditableEntityComponent.Cast(item);
		if (!editableEntity)
			return;
		
		// Search your component in editableEntity.GetOwner()
		ARU_PlayerTeleportAreaComponent myComp = ARU_PlayerTeleportAreaComponent.Cast(editableEntity.GetOwner().FindComponent(ARU_PlayerTeleportAreaComponent));
		if (!myComp)
			return;	
		
		myComp.SetZoneId(var.GetInt());		
	}
};