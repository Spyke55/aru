class ARU_InventoryHelper
{	
	//------------------------------------------------------------------------------------------------
	static int GetStorageItemCountByResource(IEntity storageEntity, ResourceName resourceName)
	{
		int count = 0;
		if(!storageEntity || !resourceName)
			return 0;
		
		array<IEntity> outItems = {};
		ClothNodeStorageComponent clothNode = ClothNodeStorageComponent.Cast(storageEntity.FindComponent(ClothNodeStorageComponent));
		if(clothNode)
		{
			clothNode.GetAll(outItems);
			foreach (IEntity ent0 : outItems)
			{
				count = count + GetStorageItemCountByResource(ent0,resourceName);	
			}
		}
		else
		{
			BaseInventoryStorageComponent storage = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));	
			if(!storage)
				return 0;
			storage.GetAll(outItems);
			foreach (IEntity ent : outItems)
			{
				ResourceName rn = ent.GetPrefabData().GetPrefabName();
				if(rn == resourceName)
					count = count +1;	
			}			
		}				
		
		return count;
	}
	
	//------------------------------------------------------------------------------------------------
	// TODO REMOVE
	static IEntity GetStorageItemEntityByResource(IEntity storageEntity, ResourceName resourceName)
	{
		IEntity entity = null;
		if(!storageEntity || !resourceName)
			return null;
		
		array<IEntity> outItems = {};
		ClothNodeStorageComponent clothNode = ClothNodeStorageComponent.Cast(storageEntity.FindComponent(ClothNodeStorageComponent));
		if(clothNode)
		{
			clothNode.GetAll(outItems);
			foreach (IEntity ent0 : outItems)
			{
				IEntity myEnt = GetStorageItemEntityByResource(ent0,resourceName);
				if(myEnt)
				{
					entity = myEnt;
					break;
				}
			}
		}
		else
		{
			BaseInventoryStorageComponent storage = BaseInventoryStorageComponent.Cast(storageEntity.FindComponent(BaseInventoryStorageComponent));	
			if(!storage)
				return null;
			storage.GetAll(outItems);
			foreach (IEntity ent : outItems)
			{
				ResourceName rn = ent.GetPrefabData().GetPrefabName();
				if(rn == resourceName)
				{
					entity = ent;
					break;
				}	
			}			
		}				
		
		return entity;
	}
	
	//------------------------------------------------------------------------------------------------
	static int GetPrimarySlotIndexEmpty(BaseWeaponManagerComponent weaponManager, int slotIndex)
	{
		array<WeaponSlotComponent> outSlots = {};
		weaponManager.GetWeaponsSlots(outSlots);
		
		foreach (WeaponSlotComponent weaponSlot : outSlots)
		{
			string type = weaponSlot.GetWeaponSlotType();
			if(type == "primary")
			{
				int index = weaponSlot.GetWeaponSlotIndex();
				if(index != slotIndex)
					continue;
				
				IEntity ent = weaponSlot.GetWeaponEntity();
				if(!ent)
					return index;					
			}
		}	
		return -1;	
	}	
	
	//------------------------------------------------------------------------------------------------
	static WeaponSlotComponent GetUserPrimarySlotWithIndex(BaseWeaponManagerComponent weaponManager, int index)
	{
		if(!weaponManager)
			return null;

		array<WeaponSlotComponent> outSlots = {};
		weaponManager.GetWeaponsSlots(outSlots);	
		foreach (WeaponSlotComponent weaponSlot : outSlots)
		{
			if(!weaponSlot)
				continue;				
			if(weaponSlot.GetWeaponSlotType() == "primary")
				if(weaponSlot.GetWeaponSlotIndex()==index)
					return weaponSlot;					
		}									
		return null;	
	}
	
	//------------------------------------------------------------------------------------------------
	static WeaponSlotComponent GetWeaponSlotByType(BaseWeaponManagerComponent weaponManager,string catWeaponType,int index)
	{
		if(!weaponManager)
			return null;

		array<WeaponSlotComponent> outSlots = {};
		weaponManager.GetWeaponsSlots(outSlots);	
		foreach (WeaponSlotComponent weaponSlot : outSlots)
		{
			if(!weaponSlot)
				continue;
			if(weaponSlot.GetWeaponSlotType() == catWeaponType)	
			{
				if(weaponSlot.GetWeaponSlotType() == "primary")
					if(weaponSlot.GetWeaponSlotIndex()==index)
						return weaponSlot;
				else
					return weaponSlot;
			}					
		}									
		return null;			
	}
	
	//------------------------------------------------------------------------------------------------
	static void GetConfigFromArsenal(IEntity arsenalEntity, ResourceName arsenalBaseConfig, out ARU_ArsenalConfig arsenalConfig, out map<ResourceName, IEntity> cachedEntities)
	{
		cachedEntities = new map<ResourceName, IEntity>();
		arsenalConfig = null;
		if(!arsenalEntity || !arsenalBaseConfig)
			return;
		
		// Creating config instance		
		Resource container = BaseContainerTools.LoadContainer(arsenalBaseConfig);
		if (!container || !container.IsValid())
		{
			ARU_Debug.MyPrint("Virtual Arsenal",string.Format("Core config '%1' not found!", arsenalBaseConfig),ARU_EDiagMenu.ModMenu_VirtualArsenal);
			return;
		}
		
		Managed instance = BaseContainerTools.CreateInstanceFromContainer(container.GetResource().ToBaseContainer());
		arsenalConfig = ARU_ArsenalConfig.Cast(instance);		
		
		// Getting SCR_ArsenalComponent allowed items.
		SCR_ArsenalComponent arsenalComp = SCR_ArsenalComponent.Cast(arsenalEntity.FindComponent(SCR_ArsenalComponent));
		if(!arsenalComp)
			return;
		
		array<SCR_ArsenalItem> arsenalItems = {};
		arsenalComp.GetFilteredArsenalItems(arsenalItems);		
		if(arsenalItems.IsEmpty())
			return;
		
		//ItemPreviewManager variables
		ChimeraWorld chimeraWorld = GetGame().GetWorld();
		ItemPreviewManagerEntity itemPreviewManagerEntity = chimeraWorld.GetItemPreviewManager();
		
		// Populating the Virtual Arsenal Config
		foreach(SCR_ArsenalItem arsenalItem : arsenalItems)
		{
			if(!arsenalItem)
				continue;
			
			ResourceName resourceName = arsenalItem.GetItemResourceName();
			if(!resourceName)
			{
				ARU_Debug.MyPrint("VirtualArsenal","Arsenal item without resourceName, will not be added into Virtual Arsenal",ARU_EDiagMenu.ModMenu_VirtualArsenal);
				continue;
			}
			
			// Creating preview entities while adding it to the map array.	
			IEntity ent = itemPreviewManagerEntity.ResolvePreviewEntityForPrefab(resourceName);
			cachedEntities.Insert(resourceName,ent);			

			string categoryName = string.Empty;
			typename catListType;
			typename catType;
			bool added = arsenalConfig.AddItemByCategoryType(arsenalItem,ent,categoryName,catListType,catType);			
			if(!added)
				ARU_Debug.MyPrint("VirtualArsenal",string.Format("Arsenal item Not Added: %1 [%2] [%3-%4]",resourceName,categoryName,catListType,catType),ARU_EDiagMenu.ModMenu_VirtualArsenal);			
		}
		return;
	}	
	
	//------------------------------------------------------------------------------------------------
	static string GetInventoryItemUIInfoName(IEntity ent)
	{
		string str = "";
		if(!ent)
			return str;
		
		InventoryItemComponent inventoryItemComponent = InventoryItemComponent.Cast(ent.FindComponent(InventoryItemComponent));
		if(inventoryItemComponent)
			str = inventoryItemComponent.GetUIInfo().GetName();
		else
			ARU_Debug.MyPrint("VirtualArsenal","GetInventoryItemUIInfoName - No name: "+ent.GetPrefabData().GetPrefabName(),ARU_EDiagMenu.ModMenu_VirtualArsenal);		
		return str;	
	}
	
	//------------------------------------------------------------------------------------------------
	static float GetOccupiedVolumePercentage(BaseInventoryStorageComponent storage, float occupiedSpace = 0)
	{
		if (!storage)
			return -1;

		float occupiedVolumePercantage;
		float capacity = GetMaxVolumeCapacity(storage);
		
		if (capacity == 0)
			return 0;
		
		if (occupiedSpace != 0)
			occupiedVolumePercantage = Math.Round(occupiedSpace / (capacity / 100));
		else
			occupiedVolumePercantage = Math.Round(GetOccupiedVolume(storage) / (capacity / 100));
		
		return occupiedVolumePercantage;
	}
	
	//------------------------------------------------------------------------------------------------
	static float GetOccupiedVolume(BaseInventoryStorageComponent storage)
	{
		if (!storage)
			return -1;

		float fOccupied = 0.0;
		if (ClothNodeStorageComponent.Cast(storage))
		{
			array<BaseInventoryStorageComponent> pOwnedStorages = {};
			storage.GetOwnedStorages(pOwnedStorages, 1, false);
			foreach (BaseInventoryStorageComponent pSubStorage : pOwnedStorages)
				if (SCR_UniversalInventoryStorageComponent.Cast(pSubStorage))
					fOccupied += pSubStorage.GetOccupiedSpace();
		}
		else
		{
			fOccupied = storage.GetOccupiedSpace();
		}
		
		return fOccupied;
	}
	
	//------------------------------------------------------------------------------------------------
	static float GetMaxVolumeCapacity(BaseInventoryStorageComponent storage)
	{
		if (!storage)
			return -1;

		float fMaxCapacity = 0.0;
		if (ClothNodeStorageComponent.Cast(storage))
		{
			array<BaseInventoryStorageComponent> pOwnedStorages = {};
			storage.GetOwnedStorages(pOwnedStorages, 1, false);
			foreach (BaseInventoryStorageComponent pSubStorage : pOwnedStorages)
				if (SCR_UniversalInventoryStorageComponent.Cast(pSubStorage))
					fMaxCapacity += pSubStorage.GetMaxVolumeCapacity();
		}
		else
		{
			fMaxCapacity = storage.GetMaxVolumeCapacity();
		}
		
		return fMaxCapacity;
	}
	
 	//------------------------------------------------------------------------------------------------
	static float GetTotalRoundedUpWeight(BaseInventoryStorageComponent storage)
	{
		if (!storage)
			return -1;
		
		float totalWeight = storage.GetTotalWeight();
		
		totalWeight = Math.Round(totalWeight * 100); // totalWeight * 100 here to not lose the weight precision
		
		return totalWeight / 100;
	}
	
	//------------------------------------------------------------------------------------------------
	static float GetItemSupplyCost(SCR_Faction faction, ResourceName resourceName, SCR_ResourceConsumer consumer)
	{
		float supplyCost;
		
		SCR_EntityCatalogManagerComponent entityCatalogManager = SCR_EntityCatalogManagerComponent.GetInstance();
		if (!entityCatalogManager)
			return 0;		
		
		SCR_EntityCatalogEntry entry;
		if (faction)
			entry = entityCatalogManager.GetEntryWithPrefabFromFactionCatalog(EEntityCatalogType.ITEM, resourceName, faction);
		else 
			entry = entityCatalogManager.GetEntryWithPrefabFromCatalog(EEntityCatalogType.ITEM, resourceName);
		
		if (!entry)
			return 0;
		
		SCR_ArsenalItem data = SCR_ArsenalItem.Cast(entry.GetEntityDataOfType(SCR_ArsenalItem));
		if (!data)
			return 0;
				
		//if (arsenalComponent)
			//supplyCost = data.GetSupplyCost(arsenalComponent.GetSupplyCostType());
		//else 
		supplyCost = data.GetSupplyCost(SCR_EArsenalSupplyCostType.DEFAULT);
		
		if (!consumer)
			return supplyCost;
		
		supplyCost = supplyCost * consumer.GetBuyMultiplier();
		
		return supplyCost;
	}				
}