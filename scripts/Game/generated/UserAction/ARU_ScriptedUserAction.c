class ARU_ScriptedUserAction: ScriptedUserAction
{			
	//------------------------------------------------------------------------------------------------
	override bool CanBeShownScript(IEntity user)
	{		
		return true;
	}
	
	//------------------------------------------------------------------------------------------------
	override bool CanBePerformedScript(IEntity user)
	{
		return true;
	}
	
	//------------------------------------------------------------------------------------------------
	override bool HasLocalEffectOnlyScript()
	{
		return true;
	}	
};

class ARU_ScriptedUserAction_UI_PlayerTeleport: ARU_ScriptedUserAction
{		
	[Attribute(defvalue: "-1", uiwidget: UIWidgets.EditBox, desc: "Teleport ID")]
	protected int m_iTeleportId;
	
	//------------------------------------------------------------------------------------------------
	override void PerformAction(IEntity pOwnerEntity, IEntity pUserEntity) 
	{	
		MenuBase myMenu = GetGame().GetMenuManager().OpenMenu(ChimeraMenuPreset.ARU_PlayerTeleport_ID); 
		ARU_UI_PlayerTeleport myMenuUI = ARU_UI_PlayerTeleport.Cast(myMenu);
		myMenuUI.SetOwner(pOwnerEntity);
		myMenuUI.Init();
	}		
};

class ARU_ScriptedUserAction_UI_VirtualArsenal: ARU_ScriptedUserAction
{
	[Attribute("", UIWidgets.ResourceNamePicker, desc: "File that contains base config (without prefabs)", params: "conf")]
	ResourceName m_ArsenalBaseConfig;	
	
	[Attribute()]
	protected ref ARU_ArsenalConfig m_ArsenalConfig;	
	
	//------------------------------------------------------------------------------------------------
	override void PerformAction(IEntity pOwnerEntity, IEntity pUserEntity) 
	{	
		if(!m_ArsenalBaseConfig)
		{
			ARU_Debug.MyPrint("VirtualArsenal","WARNING - Base config is null, menu will not open",ARU_EDiagMenu.ModMenu_VirtualArsenal);
			return;
		}	
		MenuBase myMenu = GetGame().GetMenuManager().OpenMenu(ChimeraMenuPreset.ARU_VirtualArsenal_ID);
		ARU_UI_VirtualArsenal myMenuUI = ARU_UI_VirtualArsenal.Cast(myMenu);		
		myMenuUI.SetStorageOwner(pOwnerEntity);
		myMenuUI.SetStorageUser(pUserEntity);
		bool loadCompleted = myMenuUI.LoadConfig(m_ArsenalBaseConfig,m_ArsenalConfig);
		if(loadCompleted)
			myMenuUI.InitMenu();
		else
			myMenuUI.Close();
	}		
};

class ARU_ScriptedUserAction_UI_Test: ARU_ScriptedUserAction
{		
	const string ACTION_myAction = "HintToggle";
	CameraManager cameraManager;
	CameraBase savedCamera;
		
	//------------------------------------------------------------------------------------------------
	override void PerformAction(IEntity pOwnerEntity, IEntity pUserEntity) 
	{
		Print("PerformAction - Init");	
		Print("PerformAction - End");
	}	
	
	// Vanilla uses something like these on: SCR_CampaignTutorialComponent::Check3rdPersonViewUsed
	void isThirdPerson(IEntity pUserEntity)
	{
		CameraHandlerComponent cameraHandler = CameraHandlerComponent.Cast(pUserEntity.FindComponent(CameraHandlerComponent));
		if (!cameraHandler)
			return;
		bool res = cameraHandler.IsInThirdPerson();
		Print(res);
	}
	
	//
	void hideVest(IEntity pUserEntity)
	{
		//BaseLoadoutManagerComponent loadoutUser = BaseLoadoutManagerComponent.Cast(pUserEntity.FindComponent(BaseLoadoutManagerComponent));
		//IEntity vest = loadoutUser.GetClothByArea(LoadoutVestArea);
		IEntity myEntity = GetGame().FindEntity("helmet_0");
		Print(myEntity.GetPrefabData().GetPrefabName());
		ParametricMaterialInstanceComponent cloth = ParametricMaterialInstanceComponent.Cast(myEntity.FindComponent(ParametricMaterialInstanceComponent));
		if (!cloth)
			return;
		cloth.SetUserAlphaTestParam(255);
	}
	
	/*
	void GetPrefabInfo(IEntity pUserEntity)
	{
		BaseLoadoutManagerComponent loadoutComp = BaseLoadoutManagerComponent.Cast(pUserEntity.FindComponent(BaseLoadoutManagerComponent));
		//IEntity ent = loadoutComp.GetClothByArea(LoadoutHeadCoverArea);
		IEntity ent = null;
		if(!ent)
			return;
		ResourceName container = ent.GetPrefabData().GetPrefabName();
		IEntityComponentSource entComp = SCR_BaseContainerTools.FindComponentSource(Resource.Load(container),BaseInventoryStorageComponent);
		
		if(entComp)
			Print("OK");
		else
			Print("ERR");		
	}
	*/
	
	void camera()
	{
		// Camera Entity
		//Entity camEntity = GetGame().SpawnEntityPrefab(Resource.Load("{43C5673F3AB5F854}Prefabs/Editor/Camera/ManualCameraVrArsenal2.et"));		
		//if(!camEntity)
  			//return;		
		//SCR_ManualCamera manCamEntity = SCR_ManualCamera.Cast(camEntity);
		//if(!manCamEntity)
  		//	return;		
		

		//Print(entComp.GetClassName());
		//BaseInventoryStorageComponent storage = BaseInventoryStorageComponent.Cast(entComp);
		
		//OpenMyPreviewCamera(pUserEntity,manCamEntity);		
	}
	
	//------------------------------------------------------------------------------------------------	
	void OpenMyPreviewCamera(IEntity myEntity, SCR_ManualCamera manCamEntity) 
	{
		// Set camera pos and Angle
		//vector pos = Vector(124.82,2.1,115.91);
		vector angle = Vector(-45,-135,0);
		
		vector pos = myEntity.GetOrigin();	
		pos[0] = pos[0] + 2;
		pos[1] = pos[1] + 2;
		
		manCamEntity.SetOrigin(pos);
		manCamEntity.SetAngles(angle);
		manCamEntity.SetFOVDegree(45);
		
		manCamEntity.AttachTo(myEntity);		
			
		Print( manCamEntity.GetOrigin() );							
		
		// Getting camera manager		
		cameraManager = GetGame().GetCameraManager();
		savedCamera = cameraManager.CurrentCamera();
		cameraManager.SetCamera(manCamEntity);
		
		// Switching input
        GetGame().GetInputManager().AddActionListener(ACTION_myAction, EActionTrigger.DOWN, CloseMyPreviewCamera);					    
	}	
	
	//------------------------------------------------------------------------------------------------	
	void CloseMyPreviewCamera() 
	{
		if(!cameraManager || !savedCamera)
			return;
		// Returning to the same camara that was being used before oppening MyPreviewCamera
		//cameraManager.SetCamera(savedCamera);	// Not working? why?
		cameraManager.SetPreviousCamera();
		
        // Removing switching input
        GetGame().GetInputManager().RemoveActionListener(ACTION_myAction, EActionTrigger.DOWN, CloseMyPreviewCamera);					    
	}		
};