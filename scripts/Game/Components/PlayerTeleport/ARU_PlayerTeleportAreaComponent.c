[EntityEditorProps(category: "ARU/PlayerTeleport", description: "Allow for PlayerTeleport Area configuration")]
class ARU_PlayerTeleportAreaComponentClass : ScriptComponentClass
{

};

class ARU_PlayerTeleportAreaComponent : ScriptComponent
{
	[Attribute("TeleportName", UIWidgets.EditBox, "Teleport Name")]
	private string m_sTeleportName;
	[Attribute("0", UIWidgets.EditBox, "Teleport Id")]
	private int m_iTeleportId;
	
	//------------------------------------------------------------------------------------------------
	void ARU_PlayerTeleportAreaComponent(IEntityComponentSource src, IEntity ent, IEntity parent)
	{	
		if(m_sTeleportName == "TeleportName")
		{
			m_sTeleportName = string.Format("ALFA - %1",ARU_Generic.GetTeleportAreaArrayCount()+1);
		}
		ARU_Generic.InsertTeleportArea(this);
	}
	
	//------------------------------------------------------------------------------------------------
	void ~ARU_PlayerTeleportAreaComponent()
	{		
		ARU_Generic.RemoveTeleportArea(this);
	}
	
	//------------------------------------------------------------------------------------------------
	string GetZoneName()
	{
		return m_sTeleportName;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetZoneName(string name)
	{
		m_sTeleportName = name;
	}	
	
	//------------------------------------------------------------------------------------------------
	int GetZoneId()
	{
		return m_iTeleportId;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetZoneId(int id)
	{
		m_iTeleportId = id;
	}		
};