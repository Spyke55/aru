[EntityEditorProps(category: "ARU/PlayerTeleport", description: "Allow for PlayerTeleport configuration")]
class ARU_PlayerTeleportAccesComponentClass : ScriptComponentClass
{

};

class ARU_PlayerTeleportAccesComponent : ScriptComponent
{		
	[Attribute(defvalue: "1", uiwidget: UIWidgets.ComboBox, enums: ParamEnumArray.FromEnum(ARU_Manager_UiState))]
	private ARU_Manager_UiState m_eAccesRules;	
	[Attribute(defvalue: "1", uiwidget: UIWidgets.ComboBox, enums: ParamEnumArray.FromEnum(ARU_EN))]
	private ARU_EN m_eEnablePlayerToPlayer;
	[Attribute(defvalue: "1", uiwidget: UIWidgets.ComboBox, enums: ParamEnumArray.FromEnum(ARU_EN))]
	private ARU_EN m_eEnablePlayerToArea;	
	[Attribute(defvalue: "-1", uiwidget: UIWidgets.EditBox, desc: "Teleport ID")]
	protected int m_iTeleportId;			
	
	//------------------------------------------------------------------------------------------------
	ARU_Manager_UiState GetAccesRules()
	{
		return m_eAccesRules;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetAccesRules(ARU_Manager_UiState accesRules)
	{
		m_eAccesRules = accesRules;
	}
	
	//------------------------------------------------------------------------------------------------
	ARU_EN GetEnablePlayerToPlayer()
	{
		return m_eEnablePlayerToPlayer;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetEnablePlayerToPlayer(ARU_EN enablePlayerToPlayer)
	{
		m_eEnablePlayerToPlayer = enablePlayerToPlayer;
	}		
	
	//------------------------------------------------------------------------------------------------
	ARU_EN GetEnablePlayerToArea()
	{
		return m_eEnablePlayerToArea;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetEnablePlayerToArea(ARU_EN enablePlayerToArea)
	{
		m_eEnablePlayerToArea = enablePlayerToArea;
	}	
	
	//------------------------------------------------------------------------------------------------
	int GetTeleportId()
	{
		return m_iTeleportId;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetTeleportId(int id)
	{
		m_iTeleportId = id;
	}	
};