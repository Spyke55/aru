[BaseContainerProps(),SCR_BaseContainerCustomTitleResourceName("m_Prefab", true)]
class ARU_Arsenal_Items
{
	[Attribute("", desc: "Item name shown in UI. (if not set the item name will be used)")]
	protected string m_sDisplayName;	
	
	[Attribute("", UIWidgets.ResourcePickerThumbnail, "", "et")]
	protected ResourceName m_Prefab;		
	
	//------------------------------------------------------------------------------------------------
	string GetDisplayName()
	{
		return m_sDisplayName;
	}
	
	//------------------------------------------------------------------------------------------------
	ResourceName GetItemResourceName()
	{
		return m_Prefab;
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetItem(string displayName, ResourceName resourceName)
	{
		m_sDisplayName = displayName;
		m_Prefab = resourceName;
	}		
}