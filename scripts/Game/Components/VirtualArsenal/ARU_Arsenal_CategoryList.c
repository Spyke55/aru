[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_CategoryList
{
	//------------------------------------------------------------------------------------------------
	bool IsThisCategoryList(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return false;
	}

	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_Category> GetCategoryList()
	{
		return {};
	}	
}

[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_CategoryWeaponsList : ARU_Arsenal_CategoryList
{	
	[Attribute(desc: "Editor actions")]
	ref array<ref ARU_Arsenal_CategoryWeapon> m_categoriesWeapon;
	
	//------------------------------------------------------------------------------------------------
	override bool IsThisCategoryList(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return (arsenalItemMode == SCR_EArsenalItemMode.WEAPON)||(arsenalItemMode == SCR_EArsenalItemMode.WEAPON_VARIANTS);
	}	

	//------------------------------------------------------------------------------------------------
	override array<ref ARU_Arsenal_Category> GetCategoryList()
	{
		array<ref ARU_Arsenal_Category> catList = new array<ref ARU_Arsenal_Category>();
		foreach(ARU_Arsenal_CategoryWeapon cat : m_categoriesWeapon)
			catList.Insert(cat);
		return catList;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_CategoryWeapon> GetCategoryWeaponList()
	{
		return m_categoriesWeapon;
	}
}

[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_CategoryClothList : ARU_Arsenal_CategoryList
{	
	[Attribute(desc: "Editor actions")]
	ref array<ref ARU_Arsenal_CategoryCloth> m_categoriesCloth;
	
	//------------------------------------------------------------------------------------------------
	override bool IsThisCategoryList(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return (arsenalItemMode == SCR_EArsenalItemMode.DEFAULT);
	}
	
	//------------------------------------------------------------------------------------------------
	override array<ref ARU_Arsenal_Category> GetCategoryList()
	{
		array<ref ARU_Arsenal_Category> catList = new array<ref ARU_Arsenal_Category>();
		foreach(ARU_Arsenal_CategoryCloth cat : m_categoriesCloth)
			catList.Insert(cat);
		return catList;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_CategoryCloth> GetCategoryClothList()
	{
		return m_categoriesCloth;
	}
}

[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_CategoryStorableList : ARU_Arsenal_CategoryList
{		
	[Attribute(desc: "Editor actions")]
	ref array<ref ARU_Arsenal_CategoryStorable> m_categoriesStorable;
	
	//------------------------------------------------------------------------------------------------
	override bool IsThisCategoryList(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return ((arsenalItemType == SCR_EArsenalItemType.EQUIPMENT)||(arsenalItemMode == SCR_EArsenalItemMode.SUPPORT_STATION)||(arsenalItemMode == SCR_EArsenalItemMode.AMMUNITION)||(arsenalItemMode == SCR_EArsenalItemMode.CONSUMABLE)||(arsenalItemType == SCR_EArsenalItemType.NON_LETHAL_THROWABLE) || (arsenalItemType == SCR_EArsenalItemType.LETHAL_THROWABLE) ||(arsenalItemType == SCR_EArsenalItemType.EXPLOSIVES));
	}
	
	//------------------------------------------------------------------------------------------------
	override array<ref ARU_Arsenal_Category> GetCategoryList()
	{
		array<ref ARU_Arsenal_Category> catList = new array<ref ARU_Arsenal_Category>();
		foreach(ARU_Arsenal_CategoryStorable cat : m_categoriesStorable)
			catList.Insert(cat);
		return catList;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_CategoryStorable> GetCategoryStorableList()
	{
		return m_categoriesStorable;
	}			
}

[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_CategoryAttachmentList : ARU_Arsenal_CategoryList
{	
	[Attribute(desc: "Editor actions")]
	ref array<ref ARU_Arsenal_CategoryAttachment> m_categoriesAttachment;
	
	//------------------------------------------------------------------------------------------------
	override bool IsThisCategoryList(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return (arsenalItemMode == SCR_EArsenalItemMode.ATTACHMENT);
	}
	
	//------------------------------------------------------------------------------------------------
	override array<ref ARU_Arsenal_Category> GetCategoryList()
	{
		array<ref ARU_Arsenal_Category> catList = new array<ref ARU_Arsenal_Category>();
		foreach(ARU_Arsenal_CategoryAttachment cat : m_categoriesAttachment)
			catList.Insert(cat);
		return catList;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_CategoryAttachment> GetCategoryAttachmentList()
	{
		return m_categoriesAttachment;
	}			
}