[BaseContainerProps(configRoot:true)]
class ARU_Arsenal_Category
{
	[Attribute("myCategory", desc: "Category name shown in UI.")]
	protected string m_sDisplayName;
	
	[Attribute("", UIWidgets.FileNamePicker, "Button Image", "edds")]
	ResourceName m_ElementLayout;			
	
	[Attribute(desc: "Items placed inside this category")]
	ref array<ref ARU_Arsenal_Items> m_items;
	
	//------------------------------------------------------------------------------------------------
	string GetCategoryName()
	{
		return m_sDisplayName;
	}
	
	//------------------------------------------------------------------------------------------------
	ResourceName GetCategoryPicture()
	{
		return m_ElementLayout;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ref ARU_Arsenal_Items> GetAllCategoryItems()
	{
		return m_items;
	}
	
	//------------------------------------------------------------------------------------------------
	ARU_Arsenal_Items GetCategoryItem(int index)
	{
		if(index < 0)
			return null;
		
		if(index < m_items.Count())
			return m_items[index];
		
		return null;
	}
	
	//------------------------------------------------------------------------------------------------
	int FindResourceNameInCategory(ResourceName resourceName)
	{
		array< ref ARU_Arsenal_Items> outEntitiesCategory = {};
		
		outEntitiesCategory = GetAllCategoryItems();

		if(outEntitiesCategory.IsEmpty())
			return -1;
		
		foreach (int currentIndex, ARU_Arsenal_Items item : outEntitiesCategory)
		{
			if(resourceName == item.GetItemResourceName())
				return currentIndex;
		}						

		return -1;
	}
	
	//------------------------------------------------------------------------------------------------
	void AddItem(ResourceName resourceName)
	{
		ARU_Arsenal_Items newItem = new ARU_Arsenal_Items;
		newItem.SetItem(string.Empty,resourceName);	
		m_items.Insert(newItem);
	}									
		
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return false;
	}	
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryWeapon : ARU_Arsenal_Category
{
	[Attribute("primary", desc: "")]
	protected string m_sWeaponSlotType;
	
	[Attribute("0", desc: "")]
	protected int m_sWeaponSlotIndex;
		
	//------------------------------------------------------------------------------------------------
	string GetWeaponSlotType()
	{
		return m_sWeaponSlotType;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetWeaponSlotType(string weaponSlotType)
	{
		m_sWeaponSlotType = weaponSlotType;
	}	
	
	//------------------------------------------------------------------------------------------------
	int GetWeaponSlotIndex()
	{
		return m_sWeaponSlotIndex;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetWeaponSlotIndex(int weaponSlotIndex)
	{
		m_sWeaponSlotIndex = weaponSlotIndex;
	}
	
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		// Get info
		string weaponSlotType = string.Empty;
		int weaponSlotIndex = -1;		
		switch (arsenalItemType)
		{
			case SCR_EArsenalItemType.RIFLE: 				{weaponSlotType = "primary"; 	weaponSlotIndex = 0; 	break; };
			case SCR_EArsenalItemType.PISTOL: 				{weaponSlotType = "secondary"; 	weaponSlotIndex = 2; 	break; };
			case SCR_EArsenalItemType.ROCKET_LAUNCHER: 		{weaponSlotType = "primary"; 	weaponSlotIndex = 1; 	break; };
			case SCR_EArsenalItemType.MACHINE_GUN: 			{weaponSlotType = "primary"; 	weaponSlotIndex = 0; 	break; };
			case SCR_EArsenalItemType.SNIPER_RIFLE: 		{weaponSlotType = "primary"; 	weaponSlotIndex = 0; 	break; };				
		}		
		
		// Filter
		if((GetWeaponSlotType() == weaponSlotType) && (GetWeaponSlotIndex() == weaponSlotIndex))							
			return true;
		return false;
	}
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryCloth : ARU_Arsenal_Category
{
	[Attribute("", UIWidgets.Object)]
	protected ref LoadoutAreaType m_loadoutType;
	
	//------------------------------------------------------------------------------------------------
	LoadoutAreaType GetLoadoutAreaType()
	{
		return m_loadoutType;
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetLoadoutAreaType(LoadoutAreaType loadoutAreaType)
	{
		m_loadoutType = loadoutAreaType;
	}
	
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		// Get info
		BaseLoadoutClothComponent loadoutComp = BaseLoadoutClothComponent.Cast(entity.FindComponent(BaseLoadoutClothComponent));
		if(!loadoutComp)
			return null;
		
		LoadoutAreaType areaType = loadoutComp.GetAreaType();			
		if(!areaType)
			return null;		
		
		// Filter
		if((areaType.Type()) == (GetLoadoutAreaType().Type()))
			return true;
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable : ARU_Arsenal_Category
{			
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Ammo : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		// Filter
		if(arsenalItemMode == SCR_EArsenalItemMode.AMMUNITION)
			return true;
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Medical : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		// Filter
		if(arsenalItemType == SCR_EArsenalItemType.HEAL)
			return true;		
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Explosives : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		if(arsenalItemType == SCR_EArsenalItemType.LETHAL_THROWABLE || arsenalItemType == SCR_EArsenalItemType.NON_LETHAL_THROWABLE || arsenalItemType == SCR_EArsenalItemType.EXPLOSIVES)
			return true;		
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Radio : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		SCR_RadioComponent entComp = SCR_RadioComponent.Cast(entity.FindComponent(SCR_RadioComponent));
		if(entComp)
			return true;		
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Misc : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		SCR_BinocularsComponent entComp = SCR_BinocularsComponent.Cast(entity.FindComponent(SCR_BinocularsComponent));
		if(entComp)
			return true;
		SCR_MapGadgetComponent entComp1 = SCR_MapGadgetComponent.Cast(entity.FindComponent(SCR_MapGadgetComponent));
		if(entComp1)
			return true;
		SCR_CompassComponent entComp2 = SCR_CompassComponent.Cast(entity.FindComponent(SCR_CompassComponent));
		if(entComp2)
			return true;
		SCR_FlashlightComponent entComp3 = SCR_FlashlightComponent.Cast(entity.FindComponent(SCR_FlashlightComponent));
		if(entComp3)
			return true;
		return false;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryStorable_Unknown : ARU_Arsenal_CategoryStorable
{
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		return true;
	}				
}

[BaseContainerProps(),SCR_BaseContainerCustomTitleField("m_sDisplayName")]
class ARU_Arsenal_CategoryAttachment : ARU_Arsenal_Category
{
	[Attribute("", UIWidgets.Object)]
	protected ref BaseAttachmentType m_attachmentType;
	
	//------------------------------------------------------------------------------------------------
	BaseAttachmentType GetAttachmentType()
	{
		return m_attachmentType;
	}
	
	//------------------------------------------------------------------------------------------------
	void SetAttachmentType(BaseAttachmentType attachmentType)
	{
		m_attachmentType = attachmentType;
	}
	
	// FILTER METHODS
	//------------------------------------------------------------------------------------------------
	override bool CanInsert(SCR_EArsenalItemMode arsenalItemMode, SCR_EArsenalItemType arsenalItemType, IEntity entity)
	{
		// Get info					
		InventoryItemComponent invItemComp = InventoryItemComponent.Cast(entity.FindComponent(InventoryItemComponent));
	    if(!invItemComp)
	        return null;
		
		BaseItemAttributeData attributeData = invItemComp.GetAttributes().FindAttribute(AttachmentAttributes);
	    if(!attributeData)
	        return null;
		
		// TODO other attachment types?? not only weapons			
		WeaponAttachmentAttributes weaponAttachmentAttributes = WeaponAttachmentAttributes.Cast(attributeData);
	    if(!weaponAttachmentAttributes)
	        return null;
					
		BaseAttachmentType attachmentType = weaponAttachmentAttributes.GetAttachmentType();
	    if(!attachmentType)
	        return null;
		
		// Filter
		if(attachmentType.IsInherited(GetAttachmentType().Type()))				
			return true;
		return false;
	}		
}