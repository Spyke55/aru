[ComponentEditorProps(category: "GameCode/Game/InventorySystem", description: "ARU Inventory Storage Replacement Component", color: "0 255 0 255")]
class ARU_InventoryReplaceStorageComponentClass: ScriptComponentClass
{
};

enum ARU_InventoryReplaceStorageEnd
{
	NotDefined,
	CreatingNewStorage,
	WaitingForNewStorageReplication,	
	ReplacingStorages,
	CompletedOkWithAllItems,
	CompletedOkWithoutAllItems,
	Failed,
	OvertimeFailed
};

class ARU_InventoryReplaceStorageComponent : ScriptComponent
{			
	// Member variables
	protected SCR_InventoryStorageManagerComponent m_inventoryManager = null;		
	protected BaseWeaponManagerComponent m_weaponManager = null;
	protected ARU_InventoryReplaceStorageEnd m_operationStatus = ARU_InventoryReplaceStorageEnd.NotDefined;	// OwnerSide
	protected bool m_isLocked = false;
	
	// Script invokers	
	ref ScriptInvoker m_OnReplacementEnd = new ScriptInvoker();
	ref ScriptInvoker m_OnStatusChanged = new ScriptInvoker();	
	
	//######################################################################################################
	// GENERIC
	
	//------------------------------------------------------------------------------------------------
	override protected void OnPostInit(IEntity owner)
	{			
		super.OnPostInit(owner);
		
		// Initialize variables	
		m_inventoryManager = SCR_InventoryStorageManagerComponent.Cast(owner.FindComponent(SCR_InventoryStorageManagerComponent));
		m_weaponManager = BaseWeaponManagerComponent.Cast(GetOwner().FindComponent(BaseWeaponManagerComponent));
	}
	
	//######################################################################################################
	// GET/SET		
	
	//------------------------------------------------------------------------------------------------
	ARU_InventoryReplaceStorageEnd GetOperationStatus()
	{
		return m_operationStatus;
	}
	
	//------------------------------------------------------------------------------------------------
	bool IsLocked()
	{
		return m_isLocked;
	}	
	
	//------------------------------------------------------------------------------------------------
	void SetOperationStatus(ARU_InventoryReplaceStorageEnd status, bool executeOnStatusChanged = true)
	{
		m_operationStatus = status;
		if(executeOnStatusChanged)
			m_OnStatusChanged.Invoke(this,m_operationStatus);
	}
	
	//######################################################################################################
	// REPLACE	
	
	//------------------------------------------------------------------------------------------------	
	bool TryReplaceOnlyStoragesPrefab(IEntity currentStorageEntity, ResourceName newStorageResourceName)
	{
		// If there is a Replace operation running we do nothing.
		if(m_isLocked)
			return false;
		m_isLocked = true;
		
		// Getting RplId from the current storage, Updating current status and sending RPC Call to Authority.
		RplId currentStorageId = ARU_Global.GetEntityRplId(currentStorageEntity);	
		SetOperationStatus(ARU_InventoryReplaceStorageEnd.CreatingNewStorage);
		Rpc(RpcAsk_Authority_TryCreateNewStorage,currentStorageId,newStorageResourceName);
		return true;
	}			

	//------------------------------------------------------------------------------------------------
	[RplRpc(RplChannel.Reliable, RplRcver.Server)]
	protected void RpcAsk_Authority_TryCreateNewStorage(RplId currentStorageId, ResourceName newStorageResourceName)
	{
		// Spawn new Storage to equip
		IEntity newStorageEntity = GetGame().SpawnEntityPrefab(Resource.Load(newStorageResourceName));
		newStorageEntity.SetOrigin(m_inventoryManager.GetOwner().GetOrigin());		
		if(!newStorageEntity)
		{
			Server_FailedEnd();
			return;
		}
				
		// Getting old Storage to unewquip
		IEntity oldStorageEntity = ARU_Global.GetEntityByRplId(currentStorageId);
		if(!oldStorageEntity)
		{
			Server_FailedEnd();
			return;
		}
				
		// Get storages
		array<IEntity> outEntities = {};
		BaseInventoryStorageComponent oldStorage = BaseInventoryStorageComponent.Cast(oldStorageEntity.FindComponent(BaseInventoryStorageComponent));	
		BaseInventoryStorageComponent newStorage = BaseInventoryStorageComponent.Cast(newStorageEntity.FindComponent(BaseInventoryStorageComponent));
		if(!oldStorage || !newStorage)
		{
			// Items like, helmets, botts etc. without storages...
			TryMoveItems(oldStorageEntity,newStorageEntity,outEntities);
			return;
		}		
		
		// Old storage is a ClothNode?
		ClothNodeStorageComponent oldClothNode = ClothNodeStorageComponent.Cast(oldStorageEntity.FindComponent(ClothNodeStorageComponent));	
			
		// Get items from old storage
		int count = m_inventoryManager.GetAllItems(outEntities,oldStorage);
		
		// Remove items from old
		if(!outEntities.IsEmpty())
		{
			int failedCount = 0;
			foreach(IEntity ent : outEntities)
			{
				if(oldClothNode)
				{
					InventoryItemComponent itemComp = InventoryItemComponent.Cast(ent.FindComponent(InventoryItemComponent));
					if(!itemComp)
						continue;
					BaseInventoryStorageComponent itemStorage = itemComp.GetParentSlot().GetStorage();
					if(!itemStorage)
						continue;
					bool res = m_inventoryManager.TryRemoveItemFromStorage(ent,itemStorage);				
					if(!res)
						failedCount++;		
				}
				else
				{
					bool res = m_inventoryManager.TryRemoveItemFromStorage(ent,oldStorage);				
					if(!res)
						failedCount++;							
				}
			}
			if(failedCount>0)
				Server_FailedEnd();			
		}
			
		TryMoveItems(oldStorageEntity,newStorageEntity,outEntities);						
	}		
	
	//------------------------------------------------------------------------------------------------
	// Executed on owner by RpcAsk_Owner_WaitForSpawnedItem_End			
	void TryMoveItems(IEntity oldStorageEntity, IEntity newStorageEntity, array<IEntity> outEntities)
	{							
		// Get storage
		array<RplId> outFailedEntitiesRplId = {};	
		BaseInventoryStorageComponent newStorage = BaseInventoryStorageComponent.Cast(newStorageEntity.FindComponent(BaseInventoryStorageComponent));
		if(newStorage)
		{					
			// Are those storages ClothNode?
			ClothNodeStorageComponent newClothNode = ClothNodeStorageComponent.Cast(newStorageEntity.FindComponent(ClothNodeStorageComponent));
			
			// Insert items to the new storage
			if(newClothNode)
			{				
				array<BaseInventoryStorageComponent> outEntitiesCloth = {};
				newClothNode.GetOwnedStorages(outEntitiesCloth,1,false);
				foreach(IEntity ent : outEntities)
				{
					bool isMoved = false;
					if(!ent)
						continue;
					foreach(BaseInventoryStorageComponent storage : outEntitiesCloth)
					{			
						isMoved = m_inventoryManager.TryInsertItemInStorage(ent,storage);
						if(isMoved)
							break;	
					}
					if(!isMoved)
						outFailedEntitiesRplId.Insert(ARU_Global.GetEntityRplId(ent));
				}
			}
			else
			{
				foreach(IEntity ent : outEntities)
				{
					bool isMoved = false;				
					if(!ent)
						continue;					
					isMoved = m_inventoryManager.TryInsertItemInStorage(ent,newStorage);		
				}						
			}
		}
						
		// Remove old storage
		//bool res = m_inventoryManager.TryRemoveItemFromStorage(oldStorageEntity,m_inventoryManager.GetCharacterStorage());
		bool res = m_inventoryManager.TryDeleteItem(oldStorageEntity);
		if(!res)
		{
			Server_FailedEnd();
			return;
		}	
		
		// Delete old storage
		SCR_EntityHelper.DeleteEntityAndChildren(oldStorageEntity);
		
		// Equip new storage		
		res = m_inventoryManager.EquipAny(m_inventoryManager.GetCharacterStorage(),newStorageEntity);
		if(!res)
		{
			Server_FailedEnd();
			return;
		}
		
		Rpc(RpcAsk_Owner_ReplaceStoragesDone,ARU_Global.GetEntityRplId(newStorageEntity),outFailedEntitiesRplId);	
	}
	
	//------------------------------------------------------------------------------------------------
	[RplRpc(RplChannel.Reliable, RplRcver.Owner)]	
	protected void RpcAsk_Owner_ReplaceStoragesDone(RplId newStorageId,array<RplId> outFailedEntitiesRplId)
	{	
		// Get new storage entity		
		IEntity newStorageEntity = ARU_Global.GetEntityByRplId(newStorageId);
		if(!newStorageEntity)
		{
			RpcAsk_Owner_End(ARU_InventoryReplaceStorageEnd.Failed);
			return;
		}
		
		// End replacement as CompletedXXXXXXX
		if(outFailedEntitiesRplId.Count()>0)
			RpcDo_Owner_End(ARU_InventoryReplaceStorageEnd.CompletedOkWithoutAllItems,outFailedEntitiesRplId,newStorageEntity);
		else
			RpcDo_Owner_End(ARU_InventoryReplaceStorageEnd.CompletedOkWithAllItems,outFailedEntitiesRplId,newStorageEntity);
	}	
	
	//------------------------------------------------------------------------------------------------
	protected void Server_FailedEnd()
	{
		Rpc(RpcAsk_Owner_End,ARU_InventoryReplaceStorageEnd.Failed);
	}	
	
	//------------------------------------------------------------------------------------------------
	[RplRpc(RplChannel.Reliable, RplRcver.Owner)]	
	protected void RpcAsk_Owner_End(ARU_InventoryReplaceStorageEnd code)
	{
		array<RplId> outFailedEntitiesRplId = {};
		RpcDo_Owner_End(code,outFailedEntitiesRplId);		
	}
	
	//------------------------------------------------------------------------------------------------
	protected void RpcDo_Owner_End(ARU_InventoryReplaceStorageEnd code, array<RplId> outFailedEntitiesRplId, IEntity newStorageEntity = null)
	{
		m_isLocked = false;
		SetOperationStatus(code);
		m_OnReplacementEnd.Invoke(this,m_operationStatus,outFailedEntitiesRplId,newStorageEntity);				
	}	
	
	//######################################################################################################
	// ADD ITEM	
	
	//------------------------------------------------------------------------------------------------
	void SpawnAndEquipItem(ResourceName resourceName,int actionType = 0,int categoryType = -1)
	{	
		Rpc(RpcAsk_Authority_AddItem, resourceName, actionType,categoryType);
	}		
	
	//------------------------------------------------------------------------------------------------
	[RplRpc(RplChannel.Reliable, RplRcver.Server)]
	protected void RpcAsk_Authority_AddItem(ResourceName resourceName,int actionType,int categoryType)
	{
		// Spawn item
		IEntity newEntity = GetGame().SpawnEntityPrefab(Resource.Load(resourceName));		
		if(!newEntity)
			return;

		newEntity.SetOrigin(m_inventoryManager.GetOwner().GetOrigin());
		if(actionType == 1)		
		{
			if(newEntity.FindComponent(WeaponComponent))
			{						
				if(categoryType == 0)	
				{					
					if(!m_inventoryManager.EquipAny( m_inventoryManager.GetCharacterStorage().GetWeaponStorage(), newEntity, 0 ))
						SCR_UISoundEntity.SoundEvent(SCR_SoundEvent.SOUND_INV_DROP_ERROR);
					
				}else if(categoryType == 1)
				{
					if(!m_inventoryManager.EquipAny( m_inventoryManager.GetCharacterStorage().GetWeaponStorage(), newEntity, 1 ))
						SCR_UISoundEntity.SoundEvent(SCR_SoundEvent.SOUND_INV_DROP_ERROR);							
				}
				else
					m_inventoryManager.EquipWeapon(newEntity);
			}			
		}
		else
			m_inventoryManager.TryInsertItem(newEntity);
	}	
	
	//######################################################################################################
	// REMOVE ITEM	
	
	//------------------------------------------------------------------------------------------------
	void DeleteItem(RplId ItemToDeleteId)
	{			
		Rpc(RpcAsk_Authority_DeleteItem, ItemToDeleteId);				
	}		
	
	//------------------------------------------------------------------------------------------------
	[RplRpc(RplChannel.Reliable, RplRcver.Server)]
	protected void RpcAsk_Authority_DeleteItem(RplId ItemToDeleteId)
	{					
		RpcDo_Authority_DeleteItem(ItemToDeleteId);		
	}	

	//------------------------------------------------------------------------------------------------
	protected void RpcDo_Authority_DeleteItem(RplId ItemToDeleteId)
	{
		IEntity myEnt = ARU_Global.GetEntityByRplId(ItemToDeleteId);
		bool res;
		SCR_WeaponAttachmentsStorageComponent weaponComp = SCR_WeaponAttachmentsStorageComponent.Cast(myEnt.FindComponent(SCR_WeaponAttachmentsStorageComponent));
		if(weaponComp)
		{
			EquipedWeaponStorageComponent weaponStorage = EquipedWeaponStorageComponent.Cast(GetOwner().FindComponent(EquipedWeaponStorageComponent));
			if(weaponStorage)			
				res = m_inventoryManager.TryRemoveItemFromInventory(myEnt,weaponStorage);			
		}
		else		
			res = m_inventoryManager.TryRemoveItemFromInventory(myEnt);				
				
		// Remove Item
		SCR_EntityHelper.DeleteEntityAndChildren(myEnt);	
	}	
}