[BaseContainerProps(configRoot:true)]
class ARU_ArsenalConfig
{
	[Attribute("ARSENAL 01", desc: "Arsenal Name")]
	protected string m_sDisplayName;
	
	[Attribute(desc: "Editor actions")]
	protected ref array<ref ARU_Arsenal_CategoryList> m_categoriesList;
	
	//------------------------------------------------------------------------------------------------
	string GetDisplayName()
	{
		return m_sDisplayName;
	}	
	
	//------------------------------------------------------------------------------------------------
	array<ARU_Arsenal_Category> GetCategoryByTypename(typename catListToFind)
	{
		array<ARU_Arsenal_Category> categoriesArray = {};
		foreach(ARU_Arsenal_CategoryList catList : m_categoriesList)
		{
			if(catList.Type() == catListToFind)
			{
				array<ref ARU_Arsenal_Category> catArray = catList.GetCategoryList();
				foreach(ARU_Arsenal_Category cat : catArray)		
					categoriesArray.Insert(cat);				
				return categoriesArray;				
			}
		}
		return categoriesArray;
	}

	//------------------------------------------------------------------------------------------------
	array<ARU_Arsenal_Category> GetAllCategories()
	{
		array<ARU_Arsenal_Category> categoriesArray = {};				
		foreach(ARU_Arsenal_CategoryList catList : m_categoriesList)
		{
			array<ref ARU_Arsenal_Category> catArray = catList.GetCategoryList();
			foreach(ARU_Arsenal_Category cat : catArray)		
				categoriesArray.Insert(cat);
		}								
		return categoriesArray;
	}	
		
	//------------------------------------------------------------------------------------------------
	array<ResourceName> GetAllArsenalItems()
	{
		array<ResourceName> resourceNameArray = {};
		foreach(ARU_Arsenal_CategoryList catList : m_categoriesList)
		{
			array<ref ARU_Arsenal_Category> catArray = catList.GetCategoryList();			
			foreach(ARU_Arsenal_Category cat : catArray)
			{
				array<ref ARU_Arsenal_Items> myItems = cat.GetAllCategoryItems();
				foreach(ARU_Arsenal_Items item : myItems)
					resourceNameArray.Insert(item.GetItemResourceName());
			}			
		}								
		return resourceNameArray;
	}
	
	//------------------------------------------------------------------------------------------------
	bool AddItemByCategoryType(SCR_ArsenalItem arsenalItem, IEntity entity, string categoryName, out typename catListType, out typename catType)
	{
		catListType = typename.Empty;
		catType = typename.Empty;
		
		SCR_EArsenalItemMode mode = arsenalItem.GetItemMode();
		SCR_EArsenalItemType type = arsenalItem.GetItemType();
		ResourceName resourceName = arsenalItem.GetItemResourceName();
		
		InventoryItemComponent inventoryComp = InventoryItemComponent.Cast(entity.FindComponent(InventoryItemComponent));
		if(!inventoryComp)
		{
			ARU_Debug.MyPrint("VirtualArsenal"," Filter warning - Item doesn't have InventoryItemComponent or components that inherit from it: "+resourceName,ARU_EDiagMenu.ModMenu_VirtualArsenal);
			return null;
		}		
				
		foreach(ARU_Arsenal_CategoryList catList : m_categoriesList)
		{
			ARU_Arsenal_Category category = null;
			if(catList.IsThisCategoryList(mode,type,entity))
			{
				catListType = catList.Type();
				array<ref ARU_Arsenal_Category> catArray = catList.GetCategoryList();			
				foreach(ARU_Arsenal_Category cat : catArray)
				{
					if(cat.CanInsert(mode,type,entity))
					{
						catType = cat.Type();
						cat.AddItem(resourceName);
						return true;						
					}					
				}
				return false;
			}			
		}
		return false;
	}				
};