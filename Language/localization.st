StringTable {
 ItemClassName "CustomStringTableItem"
 Items {
  CustomStringTableItem "{59C293496D137B3E}" {
   Id "ARU-Action_PlayerTeleport_Title"
   Target_en_us "ARU - Player Teleport"
   Target_es_es "ARU - Teleport a Jugador"
   Modified 1505925781
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59BD12A25BA4AA4C}" {
   Id "ARU-CopyEquipment_UI_Title"
   Target_en_us "Copy Equipment"
   Target_es_es "Copiar Equipamiento"
   Modified 1505563424
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{6048F4FAE1AFAA28}" {
   Id "ARU-EditableEntity_FlagPole_02_V1_Name"
   Target_en_us "ARU - Teleport Acces"
   Target_es_es "ARU - Acceso Teleport"
   Modified 1615394704
   Author "range"
   LastChanged "range"
  }
  CustomStringTableItem "{6048F4F9E313F67E}" {
   Id "ARU-EditableEntity_PlayerTeleportArea_01_Name"
   Target_en_us "ARU - Player Teleport Area"
   Target_es_es "ARU - Área de Teleport"
   Modified 1615395151
   Author "range"
   LastChanged "range"
  }
  CustomStringTableItem "{59AF41CDEF083518}" {
   Id "ARU-Manager_UI_Title"
   Target_en_us "ARU Manager"
   Target_es_es "Administracion de ARU"
   Modified 1504658123
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59AD14FDF4EDE996}" {
   Id "ARU-PlayerTeleport_UI_BlockedInfoText"
   Target_en_us "This menu is blocked"
   Target_es_es "El menú ha esta blockeado"
   Modified 1504523019
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59AEF280E1A06184}" {
   Id "ARU-PlayerTeleport_UI_NoPlayers"
   Target_en_us "Players not found"
   Target_es_es "No se han encontrado jugadores"
   Modified 1507270756
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59D71D0091D0B599}" {
   Id "ARU-PlayerTeleport_UI_NoZones"
   Target_en_us "Predefined zones not found"
   Target_es_es "No se han encontrado zonas predefinidas"
   Modified 1507270794
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59D927908450ED1D}" {
   Id "ARU-PlayerTeleport_UI_Null"
   Target_en_us "All tabs are disabled"
   Target_es_es "Todas las pestañas están deshabilitadas"
   Modified 1507406210
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59A9209D73787DF3}" {
   Id "ARU-PlayerTeleport_UI_NumPlayers"
   Target_en_us "Number of players:"
   Target_es_es "Número de jugadores:"
   Modified 1504256852
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59A91BB27E1EF0C2}" {
   Id "ARU-PlayerTeleport_UI_SelectPlayer"
   Target_en_us "Select a player"
   Target_es_es "Selecciona a un jugador"
   Modified 1504256119
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{59A6DE29DB749C13}" {
   Id "ARU-PlayerTeleport_UI_Title"
   Target_en_us "Player Teleport"
   Target_es_es "Teleport a Jugador"
   ActorGender NONE
   Modified 1504109746
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{5EBA90D22B5399F7}" {
   Id "ARU-VirtualArsenal_UI_Add"
   Target_en_us "Add"
   Target_es_es "Añadir"
   Modified 1589288426
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{5E9E7E6579D2FC6C}" {
   Id "ARU-VirtualArsenal_UI_Disarm"
   Target_en_us "Disarm"
   Target_es_es "Desarmar"
   Modified 1587445419
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{5EBA90D22809C9BE}" {
   Id "ARU-VirtualArsenal_UI_Remove"
   Target_en_us "Remove"
   Target_es_es "Eliminar"
   Modified 1589288437
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
  CustomStringTableItem "{5D3B39A72F882DC1}" {
   Id "ARU-VirtualArsenal_UI_Undress"
   Target_en_us "Undress"
   Target_es_es "Desvestir"
   Modified 1587445442
   Author "Oscar-PC"
   LastChanged "Oscar-PC"
  }
 }
}